INSERT INTO wd_document_types (name, created_at, modified_at) values ('all types', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('text', 1, true, 1, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('number', 2, true, 1, 2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('flag', 3, true, 1, 3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('date', 4, true, 1, 4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('time', 5, true, 1, 5, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('datetime', 6, true, 1, 6, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('tags', 7, true, 1, 7, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('id', 8, true, 1, 8, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_attributes (name, order_id, required, document_type_id, datatype_id, created_at, modified_at) values ('sequence', 9, true, 1, 9, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);