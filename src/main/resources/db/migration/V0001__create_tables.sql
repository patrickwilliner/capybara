CREATE TABLE wd_document_types (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  name VARCHAR(128) NOT NULL,
  description VARCHAR(2048)
);

CREATE TABLE wd_attributes (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  name VARCHAR(128) NOT NULL,
  description VARCHAR(2048),
  order_id INT NOT NULL,
  required BOOLEAN NOT NULL,
  document_type_id INT NOT NULL,
  datatype_id INT NOT NULL
);

CREATE TABLE wd_tags (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  name VARCHAR(128) NOT NULL
);

CREATE TABLE wd_documents (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  document_type_id INT NOT NULL
);

CREATE TABLE wd_attribute_values (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  value VARCHAR(1024) NOT NULL,

  attribute_id INT NOT NULL,
  document_id INT NOT NULL
);

CREATE TABLE wd_documents_to_tags (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  document_id INT NOT NULL,
  tag_id INT NOT NULL
);

CREATE TABLE wd_roles (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  name VARCHAR(64) NOT NULL,
  authority VARCHAR(24) NOT NULL,
  description VARCHAR(256),
  system BOOLEAN NOT NULL
);

CREATE TABLE wd_users (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  name VARCHAR(64) NOT NULL,
  email VARCHAR(128),
  login VARCHAR(32) NOT NULL UNIQUE,
  password_hash VARCHAR(32),
  password_salt VARCHAR(32),
  password_date TIMESTAMP,
  last_login TIMESTAMP,
  last_login_ip TIMESTAMP,
  active BOOLEAN NOT NULL,
  change_password BOOLEAN NOT NULL,
  system BOOLEAN NOT NULL DEFAULT false,

  role_id INT NOT NULL
);

CREATE TABLE wd_datatypes (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  name VARCHAR(48) NOT NULL,
  description VARCHAR(256),
  type VARCHAR(16) UNIQUE NOT NULL,
  format VARCHAR(256) NOT NULL,
  active BOOLEAN NOT NULL
);

CREATE TABLE wd_settings (
  id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  created_at TIMESTAMP NOT NULL,
  modified_at TIMESTAMP NOT NULL,

  setting_key VARCHAR(32) NOT NULL,
  setting_value VARCHAR(128) NOT NULL,
  description VARCHAR(512)
);

ALTER TABLE wd_documents ADD CONSTRAINT fk_document_to_document_type
FOREIGN KEY (document_type_id) REFERENCES wd_document_types(id);

ALTER TABLE wd_attribute_values ADD CONSTRAINT fk_attribute_value_to_attibute
FOREIGN KEY (attribute_id) REFERENCES wd_attributes(id);

ALTER TABLE wd_documents_to_tags ADD CONSTRAINT fk_dtt_to_document
FOREIGN KEY (document_id) REFERENCES wd_documents(id);

ALTER TABLE wd_documents_to_tags ADD CONSTRAINT fk_dtt_to_tag
FOREIGN KEY (tag_id) REFERENCES wd_tags(id);

ALTER TABLE wd_attributes ADD CONSTRAINT fk_attribute_to_document_type
FOREIGN KEY (document_type_id) REFERENCES wd_document_types(id);

ALTER TABLE wd_users ADD CONSTRAINT fk_user_to_role
FOREIGN KEY (role_id) REFERENCES wd_roles(id);

ALTER TABLE wd_attributes ADD CONSTRAINT fk_attribute_to_datatype
FOREIGN KEY (datatype_id) REFERENCES wd_datatypes(id);