INSERT INTO wd_roles (name, authority, description, system, created_at, modified_at) values ('Administrator', 'ROLE_ADMIN', 'Application Administrator Role', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_roles (name, authority, description, system, created_at, modified_at) values ('User', 'ROLE_USER', 'Application User Role', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO wd_users (name, login, password_hash, password_salt, password_date, active, change_password, system, role_id, created_at, modified_at) values ('Administrator', 'admin', 'admin', '', CURRENT_TIMESTAMP, true, false, true, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('Text', 'Free text', 'TEXT', '.*', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('Number', 'Sequence of numbers', 'NUMBER', '[0-9]*', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('Flag', 'Yes or No', 'FLAG', '', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('Date', 'Calendar Date', 'DATE', 'dd-MM-yyyy', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('Time', 'Time', 'TIME', 'HH:mm', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('DateTime', 'Date and time', 'DATETIME', 'dd-MM-yyyy HH:mm', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('Tags', 'Tags', 'TAGS', '', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('ID', 'Identification Number', 'ID', '', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO wd_datatypes (name, description, type, format, active, created_at, modified_at) values ('Sequence', 'Sequence Number', 'SEQUENCE', '', true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);