/**
 * Augment project namespace.
 * @param ns project namespace.
 */
var WDOCS = (function(ns) {
    var endlessPage = {
        checkScroll: function(ajaxUrl) {
            if (this.nearBottomOfPage()) {
                $.ajax({
                    url: ajaxUrl,
                    dataType: 'script'
                });
            } else {
                setTimeout(function() {WDOCS.endlessPage.checkScroll(ajaxUrl);}, 250);
            }
        },

        nearBottomOfPage: function() {
            return this.scrollDistanceFromBottom() < 150;
        },

        scrollDistanceFromBottom: function(argument) {
            return this.pageHeight() - (window.pageYOffset + self.innerHeight);
        },

        pageHeight: function() {
            return Math.max(document.body.scrollHeight, document.body.offsetHeight);
        },

        getUrlParameters: function() {
            var url = window.location.href;
            var result = {};
            var idx = url.indexOf("?");
            if (idx == -1 ) return result;
            else {
                var sPageURL = url.substring(idx + 1);
                var sURLVariables = sPageURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++)
                {
                    var sParameterName = sURLVariables[i].split('=');
                    result[sParameterName[0]] = sParameterName[1];
                }
                return result;
            }
        }
    }

    ns.endlessPage = endlessPage;

    return ns;
}(WDOCS));