/**
 * Augment project namespace.
 * @param ns project namespace.
 */
var WDOCS = (function(ns) {
    var docType = {
        datatypes: [],

        rescueData: function() {
            var jqRows = $('table.attributes>tbody>tr');
        },

        uiNewAttribute: function() {
            var rowCount = $('table.attributes>tbody>tr').length;
            $('table.attributes>tbody').append(this.renderRow(rowCount, {name: '', description: '', required: false}, rowCount + 1));

            $('input').unbind("focus");
            $('input').on("focus", function(event) {
                $(this).parent().addClass('focus');
            });
            $('input').unbind("focusout");
            $('input').on("focusout", function(event) {
                $(this).parent().removeClass('focus');
            });
        },
        uiRemoveAttribute: function(idx) {
            $('table.attributes>tbody>tr')[idx].remove();
        },
        uiDown: function(idx) {
            if (idx < this.attributes.length) {
                var tmp = this.attributes[idx];
                this.attributes[idx] = this.attributes[idx+1];
                this.attributes[idx+1] = tmp;
                this.render();
            }
        },
        uiUp: function(idx) {
            if (idx > 0) {
                var tmp = this.attributes[idx];
                this.attributes[idx] = this.attributes[idx-1];
                this.attributes[idx-1] = tmp;
                this.render();
            }
        },
        render: function() {
            $('table.attributes>tbody').empty();
            for (var i=0; i<this.attributes.length; i++) {
                $('table.attributes>tbody').append(this.renderRow(i, this.attributes[i], this.attributes.length));
            }
        },
        renderRow: function(id, attribute, rowCount) {
            var html = '<tr><td class="edit">';
            html += '<input type="hidden" name="attributes[' + id + '].orderId" value="" />';
            html += '<input type="text" placeholder="Enter Name" name="attributes[' + id + '].name" value="';
            html += attribute.name;
            html += '" /></td><td class="edit">';
            html += '<input type="text" placeholder="Enter Description" name="attributes[' + id + '].description" value="';
            html += attribute.description;
            html += '" /></td><td style="text-align: center;">';

            html += '<input type="hidden" name="_attributes[' + id + '].required" value="false" />';
            html += '<input type="checkbox" name="attributes[' + id + '].required" value="true" />';
            html += '</td><td class="edit">';

            html += '<select name="attributes[' + id + '].datatype.id" class="">';

            for (var i=0; i<DATA.datatypes.length; i++) {
                html += '<option value="' + DATA.datatypes[i].id + '">' + DATA.datatypes[i].name + '</option>';
            }

            html += '</select></td><td>';

            if (rowCount > 1) {
                html += '<a href="#" onclick="WDOCS.docType.uiRemoveAttribute(' + id + ');"><span class="glyphicon glyphicon-trash"></span></a>';
            }

//            if (idx > 0) {
//                html += '&nbsp;<a href="#" onclick="WDOCS.docType.uiUp(' + idx + ');"><span class="glyphicon glyphicon-chevron-up"></span></a>';
//            }
//
//            if (idx < attrCount - 1) {
//                html += '&nbsp;<a href="#" onclick="WDOCS.docType.uiDown(' + idx + ');"><span class="glyphicon glyphicon-chevron-down"></span></a>';
//            }

            html += '</td></tr>';
            return html;
        }
    }

    ns.docType = docType;

    return ns;
}(WDOCS));