/**
 * jQuery stuff.
 */
(function(code) {
    code(window.jQuery, window, document);
}(function($, window, document) {
    // document ready
    $(function() {
        WDOCS.endlessPage.checkScroll('/wdocs/documents/load_page/1');

        $('select#combo_document_type').on("change", function() {
            window.location.search = 'documentTypeId=' + this.value;
        });
    });
}));