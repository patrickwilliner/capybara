/**
 * Project namespace.
 */
var WDOCS = (function () {
    var ns = {};

    function isDate(event, source) {
        var charCode = (event.which) ? event.which : event.keyCode;

        if (source.val().length == 10) {
            return charCode < 32;
        } else {
            return (charCode < 32) || (charCode > 47 && charCode < 58) || (charCode == 45);
        }
    }

    ns.isDate = isDate;

    return ns;
}());

/**
 * jQuery stuff.
 */
(function(code) {
    code(window.jQuery, window, document);
}(function($, window, document) {
    // document ready
    $(function() {
        // change http method on links with data-method attribute set
        $('a[data-method]').append(function() {
            return "\n"+
                "<form action='"+$(this).attr('href')+"' method='POST' style='display:none'>"+
                "<input type='hidden' name='_method' value='"+$(this).attr('data-method')+"'>"+
                "</form>"
        }).removeAttr('href').attr('style','cursor:pointer;').attr('onclick','if (confirm("Are you sure")) $(this).find("form").submit();');

        $('.document.dismissable button.close').on('click', function() {
            $(this).parent().parent().remove();
        });

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            todayBtn: "linked",
            autoclose: true
        });

    });
}));