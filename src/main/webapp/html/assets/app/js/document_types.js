/**
 * jQuery stuff.
 */
(function(code) {
    code(window.jQuery, window, document);
}(function($, window, document) {
    // document ready
    $(function() {
        $('form.doctype').on("submit", function(event) {
            var jqFields = $('table.attributes input[name$=".orderId"]');
            for (var i = 0; i < jqFields.length; i++) {
                jqFields[i].value = i;
            }
        });

        WDOCS.docType.uiNewAttribute();
    });
}));