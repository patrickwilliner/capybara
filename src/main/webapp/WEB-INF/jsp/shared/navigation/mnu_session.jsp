<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">${currentUser.username} <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="#"><wd:msg key="navigation.session.change_pw" /></a>
            </li>
            <li>
                <a href="<s:url value='/logout' />"><wd:msg key="navigation.session.signout" /></a>
            </li>
        </ul>
    </li>
</ul>

<div class="navbar-header navbar-right"><div style="padding: 15px 0;"><wd:msg key="navigation.session.signed_in_as" /> </div></div>