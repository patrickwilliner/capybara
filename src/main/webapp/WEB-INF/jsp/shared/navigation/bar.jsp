<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <a href="<s:url value='/' />" class="navbar-brand">wdocs</a>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <jsp:include page="/WEB-INF/jsp/shared/navigation/mnu_documents.jsp" />
            <jsp:include page="/WEB-INF/jsp/shared/navigation/mnu_config.jsp" />
            <jsp:include page="/WEB-INF/jsp/shared/navigation/mnu_tools.jsp" />
            <jsp:include page="/WEB-INF/jsp/shared/navigation/mnu_help.jsp" />
        </ul>

        <jsp:include page="/WEB-INF/jsp/shared/navigation/mnu_session.jsp" />
    </div>
</div>