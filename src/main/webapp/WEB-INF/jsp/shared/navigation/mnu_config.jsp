<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><li>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><wd:msg key="navigation.configuration" /> <b class="caret"></b></a>
    <ul class="dropdown-menu">
        <li>
            <a href="<s:url value='/document_types' />"><wd:msg key="navigation.configuration.document_types" /></a>
        </li>

        <li>
            <a href="<s:url value='/datatypes' />"><wd:msg key="navigation.configuration.data_types" /></a>
        </li>

        <li class="divider"></li>

        <li>
            <a href="<s:url value='/roles' htmlEscape='true'/>"><wd:msg key="navigation.configuration.roles" /></a>
        </li>

        <li>
            <a href="<s:url value='/users' htmlEscape='true'/>"><wd:msg key="navigation.configuration.users" /></a>
        </li>
    </ul>
</li>