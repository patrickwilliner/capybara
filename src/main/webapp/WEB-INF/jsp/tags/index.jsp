<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>List Tags <small><wd:pluralize cardinality="${tags.size()}" word="item" /></small></h3>
        </div>

        <wd:flash_message />

        <c:forEach var="tag" items="${tags}">
            <div class="tag">

            </div>
        </c:forEach>
    </div>

    <%@include file="/WEB-INF/jsp/tags/_sidebar.jsp" %>
</div>