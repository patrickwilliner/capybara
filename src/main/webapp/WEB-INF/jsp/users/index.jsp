<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <wd:page_header header="entity.users.action.index.title" subheader="entity.users.action.index.subtitle" cardinality="${users.size()}" />

        <wd:flash_message />

        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Login</th>
                <th>Role</th>
                <th class="is_active">Active</th>
                <th class="system">System</th>

            </tr>
            </thead>
            <tbody>
            <c:forEach var="user" items="${users}">
                <tr>
                    <td class="actions">
                        <a href="<s:url value='/users/${user.id}' />">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                    </td>
                    <td class="name">${user.name}</td>
                    <td class="login">${user.login}</td>
                    <td class="role">
                        <a href="<s:url value='/roles/${user.role.id}' />">${user.role.name}</a>
                    </td>
                    <td class="is_active"><wd:bool_icon boolValue="${user.active}" /></td>
                    <td class="system"><wd:bool_icon boolValue="${user.system}" /></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <%@include file="/WEB-INF/jsp/users/_sidebar.jsp" %>
</div>