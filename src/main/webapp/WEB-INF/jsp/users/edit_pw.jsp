<%@include file="/WEB-INF/jsp/shared/include.jsp" %>

<s:url var="form_action" value="/users/${user.id}/edit_pw" />

<div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Set Password <small>${user.name}</small></h3>
        </div>

        <wd:flash_message />

        <sf:form action="${form_action}" method="PATCH" class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-sm-2 control-label">Name:</label>
                <div class="col-sm-10">
                    <p class="form-control-static">${user.name}</p>
                </div>
            </div>

            <wd:field label="Password" placeholder="Enter Password" bean="${user}" field="password" />

            <wd:field label="Confirm" placeholder="Confirm Password" bean="${user}" field="passwordConfirmation" />

            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default">Cancel</button>
            </div>
        </sf:form>

    </div>

    <%@include file="/WEB-INF/jsp/users/_sidebar.jsp" %>
</div>