<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>User Details <small>${user.name}</small></h3>
        </div>

        <wd:flash_message />

        <article>
            <dl class="dl-horizontal">
                <dt>Name:</dt>
                <dd>${user.name}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Login:</dt>
                <dd>${user.login}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>E-mail:</dt>
                <dd><wd:normalize empty="true">${user.email}</wd:normalize></dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Role:</dt>
                <dd>
                    <a href="<s:url value='/roles/${user.role.id}' />">${user.role.name}</a>
                </dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Active:</dt>
                <dd><wd:bool_icon boolValue="${user.active}" /></dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>System user:</dt>
                <dd><wd:bool_icon boolValue="${user.system}" /></dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Change password:</dt>
                <dd><wd:bool_icon boolValue="${user.changePassword}" /></dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Last login:</dt>
                <dd>${user.lastLogin} - IP ${user.lastLoginIp}</dd>
            </dl>

        </article>
    </div>

    <%@include file="/WEB-INF/jsp/users/_sidebar.jsp" %>
</div>