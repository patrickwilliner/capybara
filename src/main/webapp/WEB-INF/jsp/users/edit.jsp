<%@include file="/WEB-INF/jsp/shared/include.jsp" %>

<s:url var="form_action" value="/users/${user.id}" />

<div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Edit User <small>${user.name}</small></h3>
        </div>

        <wd:flash_message />

        <sf:form action="${form_action}" method="PATCH" class="form-horizontal" role="form">
            <wd:field label="Name" placeholder="Enter Name" bean="${user}" field="name" />

            <wd:field label="Login" placeholder="Enter Login" bean="${user}" field="login" />

            <wd:field label="Email" placeholder="Enter Email" bean="${user}" field="email" />

            <div class="form-group">
                <label class="col-sm-2 control-label" for="combo_role">Role:</label>
                <div class="col-sm-10">
                    <select id="combo_role" name="role.id" class="form-control">
                        <c:forEach var="role" items="${roles}">
                            <option value="${role.id}"<c:if test="${role.id == user.role.id}"> selected</c:if>>${role.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <button type="button" class="btn btn-default">Cancel</button>
            </div>
        </sf:form>

    </div>

    <%@include file="/WEB-INF/jsp/users/_sidebar.jsp" %>
</div>