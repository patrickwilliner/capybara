<%@ page import="org.williner.wdocs.ui.ActionE" %><%
%><%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="list-group">
        <wd:sidebar_link action="<%= ActionE.INDEX %>" href="/users">
            <wd:msg key="sidebar.users.index" />
        </wd:sidebar_link>
        <wd:sidebar_link action="<%= ActionE.NEW %>" href="/users/new">
            <wd:msg key="sidebar.users.new" />
        </wd:sidebar_link>

        <c:if test="${!action.collection}">
            <wd:sidebar_link action="<%= ActionE.SHOW %>" href="/users/${user.id}">
                <wd:msg key="sidebar.users.show" />
            </wd:sidebar_link>
            <wd:sidebar_link action="<%= ActionE.EDIT %>" href="/users/${user.id}/edit">
                <wd:msg key="sidebar.users.edit" />
            </wd:sidebar_link>
            <wd:sidebar_link action="<%= ActionE.EDIT_PASSWORD %>" href="/users/${user.id}/edit_pw">
                <wd:msg key="sidebar.users.edit_pw" />
            </wd:sidebar_link>

            <c:if test="${!user.system}">
                <wd:sidebar_link action="<%= ActionE.DELETE %>" href="/users/${user.id}/delete" method="delete">
                    <wd:msg key="sidebar.users.delete" />
                </wd:sidebar_link>
            </c:if>
        </c:if>
    </div>
</div>