<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><c:import url="/WEB-INF/jsp/documents/_documents.jsp" var="docs"/><%
%>$('#document_list').append('${wd:escapeJS(docs)}');

<c:choose>
    <c:when test="${page + 1 < pageCount}">
        WDOCS.endlessPage.checkScroll('/wdocs/documents/load_page/${page + 1}');
    </c:when>
    <c:otherwise>
        $('#load_more').remove();
    </c:otherwise>
</c:choose>