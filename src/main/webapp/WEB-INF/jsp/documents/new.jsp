<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>New Document</h3>
        </div>

        <wd:flash_message />

        <form class="form-horizontal" role="form" action="<s:url value='/documents' />" method="POST">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="combo_document_type">Document Type:</label>
                <div class="col-sm-10">
                    <select id="combo_document_type" name="documentType" size="1" class="form-control">
                        <c:forEach var="dt" items="${document_types}">
                            <option value="${dt.id}" <c:if test="${dt.id == document.documentType.id}">selected="selected"</c:if>>${dt.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <c:forEach var="attributeValue" items="${document.attributeValues}" varStatus="idx">
                <%@include file="/WEB-INF/jsp/documents/_attribute.jsp" %>
            </c:forEach>

            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <button type="button" class="btn btn-default">Cancel</button>
            </div>
        </form>
    </div>

    <%@include file="/WEB-INF/jsp/documents/_sidebar.jsp" %>

</div>