<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <wd:page_header header="entity.documents.action.index.title" subheader="entity.documents.action.index.subtitle" cardinality="${documentCount}" />

        <wd:flash_message />

        <div id="document_list">
            <%@include file="/WEB-INF/jsp/documents/_documents.jsp" %>
        </div>

        <c:set var="pg" value="${page + 1}" />
        <c:if test="${(page + 1) < pageCount}">
        <div id="load_more" class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            loading more documents&nbsp;
            <img src="<s:url value='/assets/app/img/loading.gif' />" />
        </div>
        </c:if>
    </div>

    <%@include file="/WEB-INF/jsp/documents/_sidebar.jsp" %>

</div>