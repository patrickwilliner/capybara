<%@ page import="org.williner.wdocs.ui.ActionE" %>
<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="list-group">
        <wd:sidebar_link action="<%= ActionE.INDEX %>" href="/documents">List All</wd:sidebar_link>
        <wd:sidebar_link action="<%= ActionE.SEARCH %>" href="/documents/search">Search Documents</wd:sidebar_link>
        <wd:sidebar_link action="<%= ActionE.NEW %>" href="/documents/new">Create New</wd:sidebar_link>

        <c:if test="${!action.collection}">
            <wd:sidebar_link action="<%= ActionE.SHOW %>" href="/documents/${document.id}">Show Document</wd:sidebar_link>
            <wd:sidebar_link action="<%= ActionE.EDIT %>" href="/documents/${document.id}/edit">Edit Document</wd:sidebar_link>
            <wd:sidebar_link action="<%= ActionE.DELETE %>" href="/documents/${document.id}" method="delete">Delete Document</wd:sidebar_link>
        </c:if>
    </div>
</div>