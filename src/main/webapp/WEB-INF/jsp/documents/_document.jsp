<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="document">
    <div class="pull-right">
        <a href="<s:url value='/documents/${document.id}' />">
            <span class="glyphicon glyphicon-search"></span>
        </a>
    </div>

    <c:forEach var="attributeValue" items="${document.attributeValues}">
        <dl class="dl-horizontal">
            <dt>${attributeValue.attribute.name}:</dt>
            <dd>${attributeValue.value}</dd>
        </dl>
    </c:forEach>

    <dl class="dl-horizontal">
        <dt>Document Type:</dt>
        <dd>
            <a href="<s:url value='/document_types/${document.documentType.id}' />">${document.documentType.name}</a>
        </dd>
    </dl>

    <dl class="dl-horizontal">
        <dt>Created At:</dt>
        <dd>
            <span><wd:date_format format="dd-MM-yyyy" date="${document.createdAt}" /></span>
        </dd>
    </dl>
</div>