<%@include file="/WEB-INF/jsp/shared/include.jsp" %>
<%@ page import="org.williner.wdocs.data.model.DatatypeE" %>
<c:if test="${attributeInfos[idx.index].datatype.type == 'TEXT'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'NUMBER'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" cssClass="number" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'FLAG'}">
    <wd:flagCombo label="${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'DATE'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'TIME'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'DATETIME'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'TAGS'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'ID'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>
<c:if test="${attributeInfos[idx.index].datatype.type == 'SEQUENCE'}">
    <wd:field label="${attributeInfos[idx.index].name}" placeholder="Enter ${attributeInfos[idx.index].name}" bean="${document}" field="attributeValues[${idx.index}]" />
</c:if>