<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%>

<c:choose>
    <c:when test="${documents.isEmpty()}">
        <div class="alert alert-info">
            No search result.
        </div>
    </c:when>
    <c:otherwise>
        <div id="document_listt">
            <c:forEach var="document" items="${documents}">
                <%@include file="/WEB-INF/jsp/documents/_result_document.jsp" %>
            </c:forEach>
        </div>
    </c:otherwise>
</c:choose>