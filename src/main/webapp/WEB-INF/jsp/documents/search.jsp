<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Search Documents <c:if test="${search_criteria.isSearch()}"><small><wd:pluralize cardinality="${documents.size()}" word="item" /></small></c:if></h3>
        </div>

        <wd:flash_message />

        <c:choose>
            <c:when test="${search_criteria.isSearch()}">
                <c:set var="resultBtnClass" value="active" />
                <c:set var="resultTabClass" value="in active" />
            </c:when>
            <c:otherwise>
                <c:set var="filterBtnClass" value="active" />
                <c:set var="filterTabClass" value="in active" />
                <%--TODO: disable button--%>
                <c:set var="resultBtnClass" value="disabled" />
            </c:otherwise>
        </c:choose>

        <ul class="nav nav-pills">
            <li class="${filterBtnClass}"><a href="#search_filter" data-toggle="tab">Filter</a></li>
            <li class="${resultBtnClass}"><a href="#search_result" data-toggle="tab">Results</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade ${filterTabClass}" id="search_filter">
                <%@include file="/WEB-INF/jsp/documents/_search_form.jsp" %>
            </div>
            <div class="tab-pane fade ${resultTabClass}" id="search_result">
                <%@include file="/WEB-INF/jsp/documents/_search_result.jsp" %>
            </div>
        </div>
    </div>

    <%@include file="/WEB-INF/jsp/documents/_sidebar.jsp" %>

</div>