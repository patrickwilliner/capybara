<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Document Details</h3>
        </div>

        <wd:flash_message />

        <article>
            <c:forEach var="attributeValue" items="${document.attributeValues}">
                <dl class="dl-horizontal">
                    <dt>${attributeValue.attribute.name}:</dt>
                    <dd>${attributeValue.value}</dd>
                </dl>
            </c:forEach>

            <dl class="dl-horizontal">
                <dt>Document Type:</dt>
                <dd>
                    <a href="<s:url value='/document_types/${document.documentType.id}' />" >${document.documentType.name}</a>
                </dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Created At:</dt>
                <dd>
                    <span><wd:date_format format="dd-MM-yyyy" date="${document.createdAt}" /></span>
                </dd>
            </dl>

        </article>
    </div>

    <%@include file="/WEB-INF/jsp/documents/_sidebar.jsp" %>
</div>