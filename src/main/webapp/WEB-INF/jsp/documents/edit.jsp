<%@include file="/WEB-INF/jsp/shared/include.jsp" %>

<s:url var="form_action" value="/documents/${document.id}" />

<div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Edit Document</h3>
        </div>

        <wd:flash_message />

        <sf:form class="form-horizontal" role="form" action="${form_action}" method="PATCH">
            <div class="form-group">
                <label class="col-sm-2 control-label">Document Type:</label>
                <div class="col-sm-10">
                    <p class="form-control-static">${document.documentType.name}</p>
                </div>
            </div>

            <c:forEach var="attributeValue" items="${document.attributeValues}" varStatus="idx">
                <%@include file="/WEB-INF/jsp/documents/_attribute.jsp" %>
            </c:forEach>

            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <button type="button" class="btn btn-default">Cancel</button>
            </div>
        </sf:form>
    </div>

    <%@include file="/WEB-INF/jsp/documents/_sidebar.jsp" %>

</div>