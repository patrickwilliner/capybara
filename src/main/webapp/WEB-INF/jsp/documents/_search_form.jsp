<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><form class="form-horizontal" role="form" action="<s:url value='/documents/search' />" method="GET">
    <wd:field label="Text" placeholder="Enter Text" bean="${search_criteria}" field="allFields" />

<div class="form-group">
    <label class="col-sm-2 control-label">Creation Date:</label>

    <div class="col-sm-3">
        <div class="input-group">
            <span class="input-group-addon">from</span>
            <input type="text" name="createFrom" class="form-control datepicker" placeholder="dd-mm-yyyy" style="text-align: right; width: 107px;" onkeypress="return WDOCS.isDate(event, $(this))" />
        </div>
    </div>
    <div class="col-sm-3">
        <div class="input-group">
            <span class="input-group-addon">until</span>
            <input type="text" name="createTo" class="form-control datepicker" placeholder="dd-mm-yyyy" style="text-align: right; width: 107px;" onkeypress="return WDOCS.isDate(event, $(this))" />
        </div>
    </div>
</div>

<div class="pull-right">
    <button type="submit" class="btn btn-primary">Search</button>
    <button type="button" class="btn btn-default">Cancel</button>
</div>
</form>