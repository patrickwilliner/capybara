<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<s:url value="/assets/vendor/css/normalize.css" />" />
    <link rel="stylesheet" href="<s:url value="/assets/vendor/css/bootstrap.min.css" />" />

    <link rel="stylesheet" href="<s:url value="/assets/vendor/css/datepicker.css" />" />

    <link rel="stylesheet" href="<s:url value="/assets/app/css/application.css" />" />

    <c:if test="${controller != null}">
    <link rel="stylesheet" href="<s:url value="/assets/app/css/${controller}.css" />" />
    </c:if>
</head>
<body>
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <jsp:include page="/WEB-INF/jsp/shared/navigation/bar.jsp" />

    <%--TODO: review--%>
    <script type="text/javascript">
        var DATA = (function () {
            var ns = {
                contextPath: "${pageContext.request.contextPath}"
            };

            return ns;
        }());
    </script>

    <div id="content">
        <jsp:include page="/WEB-INF/jsp/${content}.jsp" />
    </div>

    <script src="<s:url value="/assets/vendor/js/jquery.min.js" />"></script>

    <script src="<s:url value="/assets/vendor/js/bootstrap.min.js" />"></script>

    <script src="<s:url value="/assets/vendor/js/bootstrap-datepicker.js" />"></script>

    <script src="<s:url value="/assets/app/js/application.js" />"></script>

    <%--TODO: review both--%>
    <script src="<s:url value="/assets/app/js/lib/doctype.js" />"></script>
    <script src="<s:url value="/assets/app/js/lib/endless_page.js" />"></script>

    <c:if test="${controller != null}">
    <script src="<s:url value="/assets/app/js/${controller}.js" />"></script>
    </c:if>
</body>
</html>
