<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>List Datatypes <small><wd:pluralize cardinality="${datatypes.size()}" word="item" /></small></h3>
        </div>

        <wd:flash_message />

        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <th class="actions"></th>
                <th class="name">Name</th>
                <th class="description">Description</th>
                <th class="format">Format</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="datatype" items="${datatypes}">
                <tr>
                    <td class="actions">
                        <a href="<s:url value='/datatypes/${datatype.id}' />">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                    </td>
                    <td class="name">${datatype.name}</td>
                    <td class="description">${datatype.description}</td>
                    <td class="format">${datatype.format}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <%@include file="/WEB-INF/jsp/datatypes/_sidebar.jsp" %>
</div>