<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <wd:page_header header="entity.datatypes.action.show.title" />

        <wd:flash_message />

        <article>
            <dl class="dl-horizontal">
                <dt>Name:</dt>
                <dd>${datatype.name}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Description:</dt>
                <dd>${datatype.description}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Format:</dt>
                <dd>${datatype.format}</dd>
            </dl>
        </article>
    </div>

    <%@include file="/WEB-INF/jsp/datatypes/_sidebar.jsp" %>
</div>