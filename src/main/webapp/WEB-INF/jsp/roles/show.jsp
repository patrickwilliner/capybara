<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Role Details <small>${role.name}</small></h3>
        </div>

        <wd:flash_message />

        <article>
            <dl class="dl-horizontal">
                <dt>Name:</dt>
                <dd>${role.name}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Description:</dt>
                <dd>${role.description}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>System:</dt>
                <dd><wd:bool_icon boolValue="${role.system}" /></dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Users:</dt>
                <dd>${role.users.size()}</dd>
            </dl>
        </article>
    </div>

    <%@include file="/WEB-INF/jsp/roles/_sidebar.jsp" %>
</div>