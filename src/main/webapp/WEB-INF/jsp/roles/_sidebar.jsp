<%@ page import="org.williner.wdocs.ui.ActionE" %><%
%><%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="list-group">
        <wd:sidebar_link action="<%= ActionE.INDEX %>" href="/roles">List All</wd:sidebar_link>

        <c:if test="${!action.collection}">
            <wd:sidebar_link action="<%= ActionE.SHOW %>" href="/roles/${role.id}">Show Role</wd:sidebar_link>
        </c:if>
    </div>
</div>