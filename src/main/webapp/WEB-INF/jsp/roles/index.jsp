<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>List Roles <small><wd:pluralize cardinality="${roles.size()}" word="item" /></small></h3>
        </div>

        <wd:flash_message />

        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th class="actions"></th>
                    <th class="name">Name</th>
                    <th class="description">Description</th>
                    <th class="user_count">Users</th>
                    <th class="system">System</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="role" items="${roles}">
                    <tr>
                        <td class="actions">
                            <a href="<s:url value='/roles/${role.id}' />">
                                <span class="glyphicon glyphicon-search"></span>
                            </a>
                        </td>
                        <td class="name">${role.name}</td>
                        <td class="description">${role.description}</td>
                        <td class="user_count">${role.users.size()}</td>
                        <td class="system"><wd:bool_icon boolValue="${role.system}" /></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

    <%@include file="/WEB-INF/jsp/roles/_sidebar.jsp" %>
</div>