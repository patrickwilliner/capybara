<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="document_type">
    <div class="pull-right">
        <a href="<s:url value='/document_types/${documentType.id}' />">
            <span class="glyphicon glyphicon-search"></span>
        </a>
    </div>

    <dl class="dl-horizontal">
        <dt>Name:</dt>
        <dd>${documentType.name}</dd>
    </dl>

    <dl class="dl-horizontal">
        <dt>Description:</dt>
        <dd>${documentType.description}</dd>
    </dl>

    <c:forEach var="attribute" items="${documentType.attributes}">
        <dl class="dl-horizontal">
            <c:choose>
                <c:when test="${attribute.required}"><c:set var="required" value="required" /></c:when>
                <c:otherwise><c:set var="required" value="optional" /></c:otherwise>
            </c:choose>
            <dt>${attribute.name}:</dt>
            <dd>${attribute.datatype.name}, ${required}</dd>
        </dl>
    </c:forEach>
</div>