<%@include file="/WEB-INF/jsp/shared/include.jsp" %>

<s:url var="form_action" value='/document_types' />

<div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>New Document Type</h3>
        </div>

        <wd:flash_message />

        <sf:form action="${form_action}" method="POST" class="form-horizontal doctype" role="form">

            <wd:field label="Name" placeholder="Enter Name" bean="${documentType}" field="name" required="true" />

            <wd:field label="Description" placeholder="Enter Description" bean="${documentType}" field="description" />

            <div class="form-group">
                <label class="col-sm-2 control-label">Attributes:</label>
                <div class="col-sm-10">
                    <%@include file="/WEB-INF/jsp/document_types/_attribute_editor.jsp" %>
                </div>
            </div>

            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <button type="button" class="btn btn-default">Cancel</button>
            </div>
        </sf:form>
    </div>

    <%@include file="/WEB-INF/jsp/document_types/_sidebar.jsp" %>

</div>