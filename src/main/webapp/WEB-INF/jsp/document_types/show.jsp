<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Document Type Details <small>${documentType.name}</small></h3>
        </div>

        <wd:flash_message />

        <article>
            <dl class="dl-horizontal">
                <dt>Name:</dt>
                <dd>${documentType.name}</dd>
            </dl>

            <dl class="dl-horizontal">
                <dt>Description:</dt>
                <dd>${documentType.description}</dd>
            </dl>

            <c:forEach var="attribute" items="${documentType.attributes}">
                <dl class="dl-horizontal">
                    <c:choose>
                        <c:when test="${attribute.required}"><c:set var="required" value="required" /></c:when>
                        <c:otherwise><c:set var="required" value="optional" /></c:otherwise>
                    </c:choose>
                    <dt>${attribute.name}:</dt>
                    <dd>${attribute.datatype.name}, ${required}</dd>
                </dl>
            </c:forEach>
        </article>
    </div>

    <%@include file="/WEB-INF/jsp/document_types/_sidebar.jsp" %>
</div>