<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>List Document Types <small><wd:pluralize cardinality="${docTypes.size()}" word="item" /></small></h3>
        </div>

        <wd:flash_message />

        <c:forEach var="documentType" items="${docTypes}">
            <%@include file="/WEB-INF/jsp/document_types/_document_type.jsp" %>
        </c:forEach>
    </div>

    <%@include file="/WEB-INF/jsp/document_types/_sidebar.jsp" %>

</div>