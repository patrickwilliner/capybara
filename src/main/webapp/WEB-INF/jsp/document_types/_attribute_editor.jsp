<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><table class="attributes">
    <thead>
    <tr>
        <th class="name">Name</th>
        <th class="description">Description</th>
        <th class="required">Required</th>
        <th class="type">Type</th>
        <th class="actions"></th>
    </tr>
    </thead>
    <tbody />
</table>

<script type="text/javascript">
    /**
     * Augment DATA namespace.
     * @param ns DATA namespace.
     */
    var DATA = (function(ns) {
        ns.datatypes = [];

        <c:forEach var="datatype" items="${datatypes}">
        ns.datatypes.push({id: ${datatype.id}, name: '${datatype.name}'});
        </c:forEach>

        return ns;
    }(DATA));
</script>

<div>
    <button type="button" onclick="WDOCS.docType.uiNewAttribute()" class="btn btn-default btn-xs">Add Attribute</button>
</div>