<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h3>Edit Document Type</h3>
        </div>

        <wd:flash_message />

        <form class="doctype" role="form" action="<s:url value='/document_types/${documentType.id}' htmlEscape='true'/>" method="POST">
            <input type="hidden" name="attributes" />
            <div class="form-group">
                <label for="field_name">Name:</label>
                <input type="input" id="field_name" name="name" class="form-control" placeholder="Enter Name">
            </div>

            <div class="form-group">
                <label for="field_description">Description:</label>
                <input type="input" id="field_description" name="description" class="form-control" placeholder="Enter Description">
            </div>

            <label>Attributes:</label>

            <table class="table table-condensed table-hover attributes">
                <thead>
                <tr>
                    <th class="id">#</th>
                    <th class="name">Name</th>
                    <th class="description">Description</th>
                    <th class="required">Required</th>
                    <th class="type">Type</th>
                    <th class="actions"></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <td></td>
                    <td><input type="text" name="name" /></td>
                    <td><input type="text" name="description" /></td>
                    <td class="center"><input type="checkbox" name="required" /></td>
                    <td>
                        <select name="datatype" id="">
                            <c:forEach var="datatype" items="${datatypes}">
                                <option value="${datatype.id}">${datatype.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>
                        <a href="#" onclick="WDOCS.docType.uiNewAttribute();"><span class="glyphicon glyphicon-plus"></span></a>
                        <a href="#" onclick="WDOCS.docType.uiClearInput();"><span class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                </tfoot>
                <tbody>

                </tbody>
            </table>

            <div class="pull-right">
                <button type="button" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary">Save Changes</button>
            </div>
        </form>
    </div>

    <%@include file="/WEB-INF/jsp/document_types/_sidebar.jsp" %>

</div>