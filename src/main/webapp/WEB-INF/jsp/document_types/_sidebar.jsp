<%@ page import="org.williner.wdocs.ui.ActionE" %>
<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="list-group">
        <wd:sidebar_link action="<%= ActionE.INDEX %>" href="/document_types">List All</wd:sidebar_link>
        <wd:sidebar_link action="<%= ActionE.NEW %>" href="/document_types/new">Create New</wd:sidebar_link>

        <c:if test="${!action.collection}">
            <wd:sidebar_link action="<%= ActionE.SHOW %>" href="/document_types/${documentType.id}">Show Document Type</wd:sidebar_link>
            <wd:sidebar_link action="<%= ActionE.EDIT %>" href="/document_types/${documentType.id}/edit">Edit Document Type</wd:sidebar_link>
            <wd:sidebar_link action="<%= ActionE.DELETE %>" href="/document_types/${documentType.id}" method="delete">Delete Document Type</wd:sidebar_link>
        </c:if>
    </div>
</div>