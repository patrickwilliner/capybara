<%@include file="/WEB-INF/jsp/shared/include.jsp" %><%
%><div class="container">

    <form class="form-signin" role="form" action="<s:url value="/j_spring_security_check" htmlEscape="true" />" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" name="login" class="form-control" placeholder="Enter Login" required autofocus>
        <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
        <label class="checkbox">
            <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

</div>