package org.williner.wdocs.data.db;

import com.googlecode.flyway.core.Flyway;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/12/13
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */
public class MigrationRunner {
    public static void main(String[] args) {
        // Create the Flyway instance
        Flyway flyway = new Flyway();

        // Point it to the database
        flyway.setDataSource("jdbc:derby:target/wdocs;create=true", null, null);

        // Start the migration
        flyway.migrate();
    }
}
