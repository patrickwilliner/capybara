package org.williner.wdocs.data.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.williner.wdocs.data.model.UserDO;
import org.williner.wdocs.business.UserManager;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 13/01/14
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */
public class UserValidator implements Validator {
    @Autowired
    private UserManager userManager;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDO.class == clazz;
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "validation.empty");
        ValidationUtils.rejectIfEmpty(errors, "login", "validation.empty");
        validateUniqueLogin(o, errors);
    }

    private void validateUniqueLogin(Object o, Errors errors) {
        if (o != null) {
            UserDO targetUser = (UserDO)o;
            UserDO user = userManager.findByLogin(targetUser.getLogin());
            if (user != null && user.getId() != targetUser.getId()) {
                errors.rejectValue("login", "validation.unique", new String[]{targetUser.getLogin()}, "validation.unique");
            }
        }
    }
}
