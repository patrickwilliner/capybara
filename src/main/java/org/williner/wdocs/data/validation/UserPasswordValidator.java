package org.williner.wdocs.data.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.williner.wdocs.ui.model.User;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 14/01/14
 * Time: 18:44
 * To change this template use File | Settings | File Templates.
 */
public class UserPasswordValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class == clazz;
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User)target;

        if (StringUtils.isEmpty(user.getPassword())) {
            errors.rejectValue("password", "validation.empty");
        }

        if (StringUtils.isEmpty(user.getPassword())) {
            errors.rejectValue("passwordConfirmation", "validation.empty");
        }

        if (StringUtils.isNotEmpty(user.getPassword()) && !user.getPassword().equals(user.getPasswordConfirmation())) {
            errors.rejectValue("passwordConfirmation", "validation.password.no_match");
        }
    }
}
