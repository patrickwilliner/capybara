package org.williner.wdocs.data.validation;


import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.williner.wdocs.data.model.AttributeDO;
import org.williner.wdocs.data.model.DocumentTypeDO;
import org.williner.wdocs.ui.model.Document;

import java.util.*;

/**
 * Spring data validator for documents.
 *
 * @author patrick.williner@gmail.com
 * @version 1.0 | 2014-01-22
 */
public class DocumentValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Document.class == clazz;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Document document = (Document)target;
        DocumentTypeDO documentType = document.getDocumentType();

        if (documentType == null) {
            errors.rejectValue("documentType", "validation.empty");
        } else {
            List<AttributeDO> attributes = documentType.getAttributes();

            for (int i = 0; i < attributes.size(); i++) {
                String attrValue = document.getAttributeValues().get(i);

                if (attributes.get(i).getRequired() && StringUtils.isEmpty(attrValue)) {
                    errors.rejectValue("attributeValues[" + i + "]", "validation.empty");
                }
            }
        }
    }
}
