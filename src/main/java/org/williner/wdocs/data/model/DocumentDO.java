package org.williner.wdocs.data.model;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/12/13
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_documents")
public class DocumentDO extends BaseDOAbs {
    @ManyToOne
    @JoinColumn(name="document_type_id")
    private DocumentTypeDO documentType;

//    @ManyToMany
//    @ForeignKey(name = "fk_dtt_to_document", inverseName = "fk_dtt_to_tag")
    @Transient
    private List<TagDO> tags = new ArrayList();

    @OneToMany(cascade = CascadeType.ALL, mappedBy="document", fetch = FetchType.EAGER)
    private List<AttributeValueDO> attributeValues = new ArrayList();

    public DocumentDO() {
    }

    public List<AttributeValueDO> getAttributeValues() {
        if (documentType == null) {
            return new ArrayList();
        } else {
            return attributeValues;
        }
    }

    public void setAttributeValues(List<AttributeValueDO> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public List<TagDO> getTags() {
        return tags;
    }

    public void setTags(List<TagDO> tags) {
        this.tags = tags;
    }

    public DocumentTypeDO getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeDO documentType) {
        this.documentType = documentType;

        attributeValues.clear();
        for (AttributeDO attr : documentType.getAttributes()) {
            AttributeValueDO attrValue = new AttributeValueDO();
            attrValue.setDocument(this);
            attrValue.setAttribute(attr);
            attributeValues.add(attrValue);
        }
    }
}
