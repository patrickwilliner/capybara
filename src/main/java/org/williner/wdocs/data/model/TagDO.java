package org.williner.wdocs.data.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/12/13
 * Time: 21:40
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_tags")
public class TagDO extends BaseDOAbs {
    private String name;

    public TagDO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
