package org.williner.wdocs.data.model;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 27/12/13
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
public class BaseDaoAbs {
    @PersistenceContext(unitName = "entityManager")
    private javax.persistence.EntityManager entityManager;

    protected <T extends BaseDOAbs> List<T> queryAll(Class<T> clazz) {
        return entityManager.createQuery("from " + clazz.getName()).getResultList();
    }

    protected <T extends BaseDOAbs> List<T> queryAll(Class<T> clazz, int page, int pageSize) {
        int first = page * pageSize;

        Query q = entityManager.createQuery("from " + clazz.getName() + " order by id desc");
        q.setFirstResult(first).setMaxResults(pageSize);
        return q.getResultList();
    }

    protected Long aggregate(String hql) {
        return (Long)entityManager.createQuery(hql).getSingleResult();
    }

    protected <T extends BaseDOAbs> T find(Class<T> clazz, Long id) {
        if (id == null) {
            return null;
        } else {
            return entityManager.find(clazz, id);
        }
    }

    protected <T extends BaseDOAbs> List<T> find(Class<T> clazz, String hql, List<Object> parameters) {
        TypedQuery query = entityManager.createQuery(hql, clazz);
        for (int i = 0; i < parameters.size(); i++) {
            query.setParameter(i + 1, parameters.get(i));
        }
        return query.getResultList();
    }

    protected <T extends BaseDOAbs> T any(Class<T> clazz) { //TODO review -> list empty
        List<T> resultList = entityManager.createQuery("from " + clazz.getName()).setMaxResults(1).getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    protected <T extends BaseDOAbs> T persist(T entity) {
        if (entity.getId() != null) {
            entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }

        return entity;
    }

    protected <T extends BaseDOAbs> void remove(T entity) {
        entityManager.remove(entity);
    }
}
