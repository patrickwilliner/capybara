package org.williner.wdocs.data.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 19:11
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_roles")
public class RoleDO extends BaseDOAbs {
    private String name;
    private String authority;
    private String description;
    private Boolean system;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="role", fetch = FetchType.EAGER)
    private List<UserDO> users;

    public RoleDO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }

    public List<UserDO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDO> users) {
        this.users = users;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
