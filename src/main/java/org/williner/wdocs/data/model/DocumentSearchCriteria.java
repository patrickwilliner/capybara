package org.williner.wdocs.data.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 15/01/14
 * Time: 19:02
 * To change this template use File | Settings | File Templates.
 */
public class DocumentSearchCriteria {
    private String allFields;

    @DateTimeFormat(pattern="dd-MM-yyyy")
    private Date createFrom;

    @DateTimeFormat(pattern="dd-MM-yyyy")
    private Date createTo;

    public String getAllFields() {
        return allFields;
    }

    public void setAllFields(String allFields) {
        this.allFields = allFields;
    }

    public Date getCreateFrom() {
        return createFrom;
    }

    public void setCreateFrom(Date createFrom) {
        this.createFrom = createFrom;
    }

    public Date getCreateTo() {
        return createTo;
    }

    public void setCreateTo(Date createTo) {
        this.createTo = createTo;
    }

    public boolean isSearch() {
        return allFields != null;
    }
}
