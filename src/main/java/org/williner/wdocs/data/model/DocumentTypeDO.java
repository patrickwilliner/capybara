package org.williner.wdocs.data.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/12/13
 * Time: 18:33
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_document_types")
public class DocumentTypeDO extends BaseDOAbs {
    @NotEmpty(message = "validation.empty")
    @Size(max = 128, message = "validation.size.max")
    private String name;

    @Size(max = 2048, message = "validation.size.max")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="documentType", fetch = FetchType.EAGER)
    private List<AttributeDO> attributes = new ArrayList();

    public DocumentTypeDO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AttributeDO> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<AttributeDO> attributes) {
        this.attributes = attributes;
    }
}
