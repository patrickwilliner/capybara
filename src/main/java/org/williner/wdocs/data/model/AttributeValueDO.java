package org.williner.wdocs.data.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/12/13
 * Time: 21:38
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_attribute_values")
public class AttributeValueDO extends BaseDOAbs {
    private String value;

    @ManyToOne
    private AttributeDO attribute;

    @ManyToOne
    private DocumentDO document;

    public AttributeValueDO() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AttributeDO getAttribute() {
        return attribute;
    }

    public void setAttribute(AttributeDO attribute) {
        this.attribute = attribute;
    }

    public DocumentDO getDocument() {
        return document;
    }

    public void setDocument(DocumentDO document) {
        this.document = document;
    }
}
