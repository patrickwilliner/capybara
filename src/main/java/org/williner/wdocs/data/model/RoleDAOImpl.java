package org.williner.wdocs.data.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 19:24
 * To change this template use File | Settings | File Templates.
 */
public class RoleDAOImpl extends BaseDaoAbs implements RoleDAO {

    @Override
    public List<RoleDO> findAll() {
        return super.queryAll(RoleDO.class);
    }

    @Override
    public RoleDO findById(Long id) {
        return super.find(RoleDO.class, id);
    }

    @Override
    public void persist(RoleDO role) {
        super.persist(role);
    }
}
