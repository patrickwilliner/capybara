package org.williner.wdocs.data.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 19:19
 * To change this template use File | Settings | File Templates.
 */
public interface RoleDAO {
    public List<RoleDO> findAll();
    public RoleDO findById(Long id);
    public void persist(RoleDO role);
}
