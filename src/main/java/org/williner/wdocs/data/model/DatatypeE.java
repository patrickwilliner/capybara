package org.williner.wdocs.data.model;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 25/01/14
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
public enum DatatypeE {
    TEXT,
    NUMBER,
    FLAG,
    DATE,
    TIME,
    DATETIME,
    TAGS,
    ID,
    SEQUENCE;
}
