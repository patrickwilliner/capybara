package org.williner.wdocs.data.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 30/12/13
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 */
public class DatatypeDAOImpl extends BaseDaoAbs implements DatatypeDAO {
    @Override
    public List<DatatypeDO> findAll() {
        return super.queryAll(DatatypeDO.class);
    }

    @Override
    public DatatypeDO findById(Long id) {
        return super.find(DatatypeDO.class, id);
    }
}
