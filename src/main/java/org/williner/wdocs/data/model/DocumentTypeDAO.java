package org.williner.wdocs.data.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 10/12/13
 * Time: 01:06
 * To change this template use File | Settings | File Templates.
 */
public interface DocumentTypeDAO {
    public List<DocumentTypeDO> findAll();
    public DocumentTypeDO findById(Long id);
    public DocumentTypeDO findAny();
    public void persist(DocumentTypeDO entity);
    public void remove(DocumentTypeDO documentType);
}
