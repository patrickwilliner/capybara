package org.williner.wdocs.data.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 20:41
 * To change this template use File | Settings | File Templates.
 */
public interface UserDAO {
    public List<UserDO> findAll();
    public UserDO findById(Long id);
    public UserDO findByLogin(String login);
    public UserDO persist(UserDO user);
    public void remove(UserDO user);
}
