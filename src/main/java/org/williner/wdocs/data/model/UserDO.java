package org.williner.wdocs.data.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 19:14
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_users")
public class UserDO extends BaseDOAbs {
        @NotEmpty(message = "validation.empty")
        private String name;

        private String email;

        @NotEmpty(message = "validation.emptyy")
        private String login;

        @Column(name="password_hash")
        private String passwordHash;

        @Column(name="password_salt")
        private String passwordSalt;

        @Column(name="password_date")
        private Date passwordDate;

        @Column(name="last_login")
        private Date lastLogin;

        @Column(name="last_login_ip")
        private String lastLoginIp;

        private Boolean active;

        @Column(name = "change_password")
        private Boolean changePassword;

        private Boolean system;

        @ManyToOne
        @JoinColumn(name="role_id")
        private RoleDO role;

        public UserDO() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPasswordHash() {
            return passwordHash;
        }

        public void setPasswordHash(String passwordHash) {
            this.passwordHash = passwordHash;
        }

        public Date getLastLogin() {
            return lastLogin;
        }

        public void setLastLogin(Date lastLogin) {
            this.lastLogin = lastLogin;
        }

        public String getLastLoginIp() {
            return lastLoginIp;
        }

        public void setLastLoginIp(String lastLoginIp) {
            this.lastLoginIp = lastLoginIp;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public Boolean getSystem() {
            return system;
        }

        public void setSystem(Boolean system) {
            this.system = system;
        }

        public RoleDO getRole() {
            return role;
        }

        public void setRole(RoleDO role) {
            this.role = role;
        }

        public String getPasswordSalt() {
            return passwordSalt;
        }

        public void setPasswordSalt(String passwordSalt) {
            this.passwordSalt = passwordSalt;
        }

        public Date getPasswordDate() {
            return passwordDate;
        }

        public void setPasswordDate(Date passwordDate) {
            this.passwordDate = passwordDate;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Boolean getChangePassword() {
            return changePassword;
        }

        public void setChangePassword(Boolean changePassword) {
            this.changePassword = changePassword;
        }
}
