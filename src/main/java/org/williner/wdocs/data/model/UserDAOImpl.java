package org.williner.wdocs.data.model;

import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 20:46
 * To change this template use File | Settings | File Templates.
 */
public class UserDAOImpl extends BaseDaoAbs implements UserDAO {

    @Override
    public List<UserDO> findAll() {
        return super.queryAll(UserDO.class);
    }

    @Override
    public UserDO findById(Long id) {
        return super.find(UserDO.class, id);
    }

    @Override
    public UserDO findByLogin(String login) {
        String hql = "from UserDO where login = ?";
        List<UserDO> users = super.find(UserDO.class, hql, Collections.<Object>singletonList(login));
        if (users.isEmpty()) {
            return null;
        } else if (users.size() > 1) {
            throw new IllegalStateException("login must be unique");
        } else {
            return users.get(0);
        }
    }

    @Override
    public UserDO persist(UserDO user) {
        return super.persist(user);
    }

    @Override
    public void remove(UserDO user) {
        super.remove(user);
    }
}
