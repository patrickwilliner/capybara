package org.williner.wdocs.data.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 23:48
 * To change this template use File | Settings | File Templates.
 */
public interface DatatypeDAO {
    public List<DatatypeDO> findAll();
    public DatatypeDO findById(Long id);
}
