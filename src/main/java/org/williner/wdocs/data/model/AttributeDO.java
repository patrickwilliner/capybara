package org.williner.wdocs.data.model;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/12/13
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_attributes")
public class AttributeDO extends BaseDOAbs {
    private String name;
    private String description;
    private Boolean required;

    @Column(name="order_id")
    private Long orderId;

    @ManyToOne
    @JoinColumn(name="document_type_id")
    private DocumentTypeDO documentType;

    @ManyToOne
    @JoinColumn(name="datatype_id")
    private DatatypeDO datatype;

    public AttributeDO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public DocumentTypeDO getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeDO documentType) {
        this.documentType = documentType;
    }

    public DatatypeDO getDatatype() {
        return datatype;
    }

    public void setDatatype(DatatypeDO datatype) {
        this.datatype = datatype;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || other.getClass() != getClass()) {
            return false;
        } else {
            AttributeDO otherAttr = (AttributeDO)other;
            return getId() != null && getId().equals(otherAttr.getId());
        }
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : hashCode();
    }
}
