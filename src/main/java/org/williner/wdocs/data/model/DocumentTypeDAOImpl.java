package org.williner.wdocs.data.model;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 10/12/13
 * Time: 01:08
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class DocumentTypeDAOImpl extends BaseDaoAbs implements DocumentTypeDAO {

    @Override
    public List<DocumentTypeDO> findAll() {
        return super.queryAll(DocumentTypeDO.class);
    }

    @Override
    public DocumentTypeDO findById(Long id) {
        return super.find(DocumentTypeDO.class, id);
    }

    @Override
    public DocumentTypeDO findAny() {
        return super.any(DocumentTypeDO.class);
    }

    @Override
    public void persist(DocumentTypeDO entity) {
        super.persist(entity);
    }

    @Override
    public void remove(DocumentTypeDO documentType) {
        super.remove(documentType);
    }
}
