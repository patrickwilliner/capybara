package org.williner.wdocs.data.model;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 15:15
 * To change this template use File | Settings | File Templates.
 */
public class DocumentDAOImpl extends BaseDaoAbs implements DocumentDAO {
    private int pageSize;

    @Override
    public List<DocumentDO> findAll() {
        return super.queryAll(DocumentDO.class);
    }

    @Override
    public DocumentDO findById(Long id) {
        return super.find(DocumentDO.class, id);
    }

    @Override
    public List<DocumentDO> findAll(int page) {
        return super.queryAll(DocumentDO.class, page, pageSize);
    }

    @Override
    public List<DocumentDO> find(DocumentSearchCriteria criteria) {
        String hql = "" +
                "SELECT DISTINCT d " +
                "FROM DocumentDO d " +
                "JOIN d.attributeValues av " +
                "JOIN d.documentType dt " +
                "WHERE 1=1";

        List<Object> params = new ArrayList();
        int paramId = 1;

        if (StringUtils.isNotEmpty(criteria.getAllFields())) {
            hql += " AND lower(dt.name) like ?" + paramId + " OR lower(av.value) like ?" + paramId++;
            String param = StringUtils.defaultString(criteria.getAllFields());
            param = "%" + param.toLowerCase() + "%";
            params.add(param);
        }
        if (criteria.getCreateFrom() != null) {
            hql += " AND d.createdAt > ?" + paramId++;
            params.add(criteria.getCreateFrom());
        }
        if (criteria.getCreateTo() != null) {
            hql += " AND d.createdAt < ?" + paramId++;
            params.add(criteria.getCreateTo());
        }

        System.out.println(hql);

        return super.find(DocumentDO.class, hql, params);
    }

    @Override
    public long count() {
        String hql = "select count(id) from DocumentDO";
        return super.aggregate(hql);
    }

    @Override
    public void persist(DocumentDO document) {
        super.persist(document);
    }

    @Override
    public void remove(DocumentDO document) {
        super.remove(document);
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
