package org.williner.wdocs.data.model;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 23:38
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "wd_datatypes")
public class DatatypeDO extends BaseDOAbs {
    private String name;
    private String description;
    private String format;
    private Boolean active;

    @Enumerated(EnumType.STRING)
    private DatatypeE type;

    public DatatypeDO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public DatatypeE getType() {
        return type;
    }

    public void setType(DatatypeE type) {
        this.type = type;
    }
}
