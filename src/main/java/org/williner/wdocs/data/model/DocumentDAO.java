package org.williner.wdocs.data.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */
public interface DocumentDAO {
    public List<DocumentDO> findAll();
    public DocumentDO findById(Long id);
    public List<DocumentDO> findAll(int page);
    public List<DocumentDO> find(DocumentSearchCriteria criteria);
    public long count();
    public void persist(DocumentDO document);
    public void remove(DocumentDO document);
    public int getPageSize();
    public void setPageSize(int pageSize);
}
