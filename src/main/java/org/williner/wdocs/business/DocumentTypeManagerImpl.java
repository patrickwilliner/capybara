package org.williner.wdocs.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.williner.wdocs.data.model.DocumentTypeDAO;
import org.williner.wdocs.data.model.DocumentTypeDO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 10/12/13
 * Time: 01:25
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DocumentTypeManagerImpl implements DocumentTypeManager {
    @Autowired
    private DocumentTypeDAO documentTypeDAO;

    @Override
    @Transactional
    public List<DocumentTypeDO> findAll() {
        return documentTypeDAO.findAll();
    }

    @Override
    @Transactional
    public DocumentTypeDO findById(Long id) {
        return documentTypeDAO.findById(id);
    }

    @Override
    @Transactional
    public DocumentTypeDO findAny() {
        return documentTypeDAO.findAny();
    }

    @Override
    @Transactional
    public void persist(DocumentTypeDO documentType) {
        documentTypeDAO.persist(documentType);
    }

    @Override
    @Transactional
    public void remove(Long documentTypeId) {
        DocumentTypeDO documentType = documentTypeDAO.findById(documentTypeId);
        documentTypeDAO.remove(documentType);
    }
}
