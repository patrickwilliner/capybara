package org.williner.wdocs.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.williner.wdocs.data.model.RoleDAO;
import org.williner.wdocs.data.model.RoleDO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 19:27
 * To change this template use File | Settings | File Templates.
 */
public class RoleManagerImpl implements RoleManager {
    @Autowired
    private RoleDAO roleDAO;

    @Override
    @Transactional
    public List<RoleDO> findAll() {
        return roleDAO.findAll();
    }

    @Override
    @Transactional
    public RoleDO findById(Long id) {
        return roleDAO.findById(id);
    }

    @Override
    @Transactional
    public void persist(RoleDO role) {
        roleDAO.persist(role);
    }
}
