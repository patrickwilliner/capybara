package org.williner.wdocs.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.williner.wdocs.data.model.DocumentDAO;
import org.williner.wdocs.data.model.DocumentDO;
import org.williner.wdocs.data.model.DocumentSearchCriteria;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */
public class DocumentManagerImpl implements DocumentManager {
    @Autowired
    private DocumentDAO documentDAO;

    private int pageSize;

    @Override
    @Transactional
    public List<DocumentDO> findAll() {
        return documentDAO.findAll();
    }

    @Override
    @Transactional
    public DocumentDO findById(Long id) {
        return documentDAO.findById(id);
    }

    @Override
    @Transactional
    public List<DocumentDO> findAll(int page) {
        return documentDAO.findAll(page);
    }
    @Override
    @Transactional
    public List<DocumentDO> find(DocumentSearchCriteria criteria) {
        return documentDAO.find(criteria);
    }

    @Override
    @Transactional
    public long count() {
        return documentDAO.count();
    }

    @Override
    @Transactional
    public void persist(DocumentDO document) {
        documentDAO.persist(document);
    }

    @Override
    @Transactional
    public void remove(Long id) {
        DocumentDO document = documentDAO.findById(id);
        documentDAO.remove(document);
    }

    public int getPageSize() {
        return documentDAO.getPageSize();
    }
}
