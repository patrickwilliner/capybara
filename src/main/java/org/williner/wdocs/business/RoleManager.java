package org.williner.wdocs.business;

import org.williner.wdocs.data.model.RoleDO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 19:26
 * To change this template use File | Settings | File Templates.
 */
public interface RoleManager {
    public List<RoleDO> findAll();
    public RoleDO findById(Long id);
    public void persist(RoleDO role);
}
