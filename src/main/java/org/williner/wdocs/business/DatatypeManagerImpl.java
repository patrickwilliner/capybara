package org.williner.wdocs.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.williner.wdocs.data.model.DatatypeDAO;
import org.williner.wdocs.data.model.DatatypeDO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 30/12/13
 * Time: 12:47
 * To change this template use File | Settings | File Templates.
 */
public class DatatypeManagerImpl implements DatatypeManager {
    @Autowired
    private DatatypeDAO datatypeDAO;

    @Transactional
    @Override
    public List<DatatypeDO> findAll() {
        return datatypeDAO.findAll();
    }

    @Transactional
    @Override
    public DatatypeDO findById(Long id) {
        return datatypeDAO.findById(id);
    }
}
