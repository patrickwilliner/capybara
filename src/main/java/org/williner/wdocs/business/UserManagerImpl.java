package org.williner.wdocs.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.williner.wdocs.data.model.UserDAO;
import org.williner.wdocs.data.model.UserDO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 20:48
 * To change this template use File | Settings | File Templates.
 */
public class UserManagerImpl implements UserManager {
    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public List<UserDO> findAll() {
        return userDAO.findAll();
    }

    @Override
    @Transactional
    public UserDO findById(Long id) {
        return userDAO.findById(id);
    }

    @Override
    @Transactional
    public UserDO findByLogin(String login) {
        return userDAO.findByLogin(login);
    }

    @Override
    @Transactional
    public UserDO persist(UserDO user) {
        return userDAO.persist(user);
    }

    @Override
    @Transactional
    public void remove(Long userId) {
        UserDO user = userDAO.findById(userId);

        if (!user.getSystem()) {
            //TODO review
            user.getRole().getUsers().remove(user);
            userDAO.remove(user);
        } else {
            throw new IllegalArgumentException("system users cannot be deleted (id=" + userId + ")");
        }
    }
}
