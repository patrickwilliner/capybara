package org.williner.wdocs.business;

import org.williner.wdocs.data.model.UserDO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 20:42
 * To change this template use File | Settings | File Templates.
 */
public interface UserManager {
    public List<UserDO> findAll();
    public UserDO findById(Long id);
    public UserDO findByLogin(String login);
    public UserDO persist(UserDO user);
    public void remove(Long userId);
}
