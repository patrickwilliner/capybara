package org.williner.wdocs.business;

import org.williner.wdocs.data.model.DocumentTypeDO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 10/12/13
 * Time: 01:24
 * To change this template use File | Settings | File Templates.
 */
public interface DocumentTypeManager {
    public List<DocumentTypeDO> findAll();
    public DocumentTypeDO findById(Long id);
    public DocumentTypeDO findAny();
    public void persist(DocumentTypeDO documentType);
    public void remove(Long documentTypeId);
}
