package org.williner.wdocs.business;

import org.williner.wdocs.data.model.DocumentDO;
import org.williner.wdocs.data.model.DocumentSearchCriteria;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 15:05
 * To change this template use File | Settings | File Templates.
 */
public interface DocumentManager {
    public List<DocumentDO> findAll();
    public List<DocumentDO> findAll(int page);
    public List<DocumentDO> find(DocumentSearchCriteria criteria);
    public long count();
    public DocumentDO findById(Long id);
    public void persist(DocumentDO document);
    public void remove(Long id);

    public int getPageSize();
}
