package org.williner.wdocs.ui;

import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 04/01/14
 * Time: 14:36
 * To change this template use File | Settings | File Templates.
 */
public enum ActionE {
    INDEX("/", RequestMethod.GET, true),
    NEW("/new", RequestMethod.GET, true),
    CREATE("/", RequestMethod.PUT, true),
    SEARCH("/search", RequestMethod.GET, true),
    SHOW("/{id}", RequestMethod.GET, false),
    EDIT("/{id}/edit", RequestMethod.GET, false),
    UPDATE("/{id}", RequestMethod.PATCH, false),
    DELETE("/{id}", RequestMethod.DELETE, false),
    EDIT_PASSWORD("/{id}/set_pw", RequestMethod.GET, false),
    UPDATE_PASSWORD("/{id}/set_pw", RequestMethod.PATCH, false);

    private final String name;
    private final RequestMethod method;
    private final boolean collection;

    private ActionE(String name, RequestMethod method, boolean collection) {
        this.name = name;
        this.method = method;
        this.collection = collection;
    }

    public String getName() {
        return name;
    }

    public RequestMethod getMethod() {
        return method;
    }

    public boolean isCollection() {
        return collection;
    }
}
