package org.williner.wdocs.ui;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 07/01/14
 * Time: 18:21
 * To change this template use File | Settings | File Templates.
 */
public class GlobalModelAugmentInterceptor extends HandlerInterceptorAdapter {
    public GlobalModelAugmentInterceptor() {
        super();
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mav) throws Exception {
        if (mav != null) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null) {
                mav.addObject("currentUser", authentication.getPrincipal());
            }

            if (handler != null) {
                HandlerMethod handlerMethod = (HandlerMethod)handler;
                Meta metaInfo = handlerMethod.getBean().getClass().getAnnotation(Meta.class);

                if (metaInfo != null) {
                    mav.addObject("controller", metaInfo.name());
                }
            }
        }
    }
}
