package org.williner.wdocs.ui.model;

import org.williner.wdocs.data.model.DatatypeDO;
import org.williner.wdocs.data.model.DatatypeE;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 25/01/14
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 */
public class AttributeInfo {
    private final String name;
    private final boolean required;
    private final DatatypeDO datatype;

    public AttributeInfo(String name, boolean required, DatatypeDO datatype) {
        this.name = name;
        this.required = required;
        this.datatype = datatype;
    }

    public String getName() {
        return name;
    }

    public boolean isRequired() {
        return required;
    }

    public DatatypeDO getDatatype() {
        return datatype;
    }
}
