package org.williner.wdocs.ui.model;

import org.williner.wdocs.data.model.UserDO;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/01/14
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class User implements Serializable {
    private Long id;
    private String name;
    private String password;
    private String passwordConfirmation;
    private Boolean system;

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }
}
