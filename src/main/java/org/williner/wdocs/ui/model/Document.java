package org.williner.wdocs.ui.model;

import org.williner.wdocs.data.model.DocumentTypeDO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 23/01/14
 * Time: 13:37
 * To change this template use File | Settings | File Templates.
 */
public class Document implements Serializable {
    private Long id;
    private DocumentTypeDO documentType;

    private List<String> attributeValues = new ArrayList();

    public Document() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DocumentTypeDO getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeDO documentType) {
        this.documentType = documentType;
    }

    public List<String> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(List<String> attributeValues) {
        this.attributeValues = attributeValues;
    }
}
