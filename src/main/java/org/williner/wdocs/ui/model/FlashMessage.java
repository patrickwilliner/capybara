package org.williner.wdocs.ui.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 13/01/14
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
public class FlashMessage implements Serializable {
    public enum TypeE {
        INFO,
        SUCCESS,
        WARNING,
        ERROR
    }

    private final TypeE type;
    private final String message;

    public FlashMessage(TypeE type, String message) {
        this.type = type;
        this.message = message;
    }

    public TypeE getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
