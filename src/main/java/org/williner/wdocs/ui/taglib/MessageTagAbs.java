package org.williner.wdocs.ui.taglib;

import org.springframework.context.MessageSource;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.tagext.TagSupport;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 16/01/14
 * Time: 22:48
 * To change this template use File | Settings | File Templates.
 */
public abstract class MessageTagAbs extends TagSupport {
    public MessageTagAbs() {
        super();
    }

    public String getMessage(String key) {
        WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext());
        MessageSource messageSource = (MessageSource)springContext.getBean("messageSource");
        return messageSource.getMessage(key, new Object[] {}, key, pageContext.getRequest().getLocale());
    }
}
