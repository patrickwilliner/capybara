package org.williner.wdocs.ui.taglib;

import javax.servlet.jsp.JspWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 16/01/14
 * Time: 22:18
 * To change this template use File | Settings | File Templates.
 */
public class MessageTag extends MessageTagAbs {
    private String key;

    public MessageTag() {
        super();
    }

    @Override
    public int doEndTag(){
        JspWriter out = pageContext.getOut();
        try {
            out.print(getMessage(key));
        } catch (IOException e) {
            throw new RuntimeException("", e);
        }
        return SKIP_BODY;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
