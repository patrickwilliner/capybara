package org.williner.wdocs.ui.taglib.form;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.williner.wdocs.ui.taglib.MessageTagAbs;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 13/01/14
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
public class FieldTag extends FormElementTagAbs {
    private String placeholder;
    private String cssClass;

    public FieldTag() {
    }

    @Override
    public int doEndTag() {
        try {
            JspWriter out = pageContext.getOut();

            String fieldId = getFieldId();
            List<FieldError> errors = getErrors();

            out.print("<div class=\"form-group");
            if (!errors.isEmpty()) {
                out.print(" has-error");
            }
            out.println("\">");

            out.print("\t<label class=\"col-sm-2 control-label\" for=\"");
            out.print(fieldId);
            out.print("\">");
            out.print(getLabel());

            if (isRequired()) {
                out.print("*");
            }

            out.println(":</label>");

            out.println("\t<div class=\"col-sm-10\">");

            out.print("\t\t<input type=\"text\" class=\"form-control");

            if (StringUtils.isNotEmpty(cssClass)) {
                out.print(" " + cssClass);
            }

            out.print("\" id=\"");
            out.print(fieldId);
            out.print("\" name=\"");
            out.print(getField());
            out.print("\" placeholder=\"");
            out.print(placeholder);
            out.print("\" value=\"");
            out.print(getValue());
            out.println("\" />");

            if (!errors.isEmpty()) {
                String msgKey = getMsgKey(errors);
                String msg = getMessage(msgKey);

                out.print("\t\t<span class=\"help-block\">");
                out.print(msg);
                out.println("</span>");
            }

            out.println("\t</div>");
            out.println("</div>");

            return SKIP_BODY;
        } catch (IOException e) {
            throw new RuntimeException("", e);
        }
    }

    private String getMsgKey(List<FieldError> errors) {
        if (errors != null && !errors.isEmpty()) {
            FieldError fieldError = errors.get(0);
            String msgKey = fieldError.getDefaultMessage();
            if (StringUtils.isNotEmpty(msgKey)) {
                return msgKey;
            } else {
                return fieldError.getCode();
            }
        } else {
            return null;
        }
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    private String getFieldId() {
        return "field_" + getField();
    }
}
