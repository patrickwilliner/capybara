package org.williner.wdocs.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 28/12/13
 * Time: 14:10
 * To change this template use File | Settings | File Templates.
 */
public class PluralizeTag extends SimpleTagSupport {
    private Integer cardinality;
    private String word;

    public void setCardinality(Integer cardinality) {
        this.cardinality = cardinality;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.print(cardinality);
        out.print(" ");
        out.println(cardinality == 1 ? word : word + "s");
    }
}
