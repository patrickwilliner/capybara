package org.williner.wdocs.ui.taglib.form;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.williner.wdocs.ui.taglib.MessageTagAbs;

import javax.servlet.jsp.JspWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 24/01/14
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */
public class FlagTag extends FormElementTagAbs {
    public FlagTag() {
        super();
    }

    @Override
    public int doEndTag() {
        JspWriter out = pageContext.getOut();

        try {
            String fieldId = getFieldId();

            out.println("<div class=\"form-group\">");

            out.print("<label class=\"col-sm-2 control-label\" for=\"");
            out.print(fieldId);
            out.print("\">");
            out.print(getMessage(getLabel()));
            out.println(":</label>");

            out.println("<div class=\"col-sm-10\">");

            out.print("<select class=\"form-control\" id=\"");
            out.print(fieldId);
            out.print("\" name=\"");
            out.print(getField());
            out.println("\">");

            out.print("<option value=\"true\"");

            boolean value = Boolean.parseBoolean(getValue());

            if (value) {
               out.print(" selected");
            }

            out.print(">");

            out.print(getMessage("misc.true"));
            out.println("</option>");

            out.print("<option value=\"false\"");

            if (!value) {
                out.print(" selected");
            }

            out.print(">");

            out.print(getMessage("misc.false"));
            out.println("</option>");

            out.println("</select>");

            out.println("</div>");
            out.println("</div>");
        } catch (IOException e) {
            throw new RuntimeException("", e);
        }

        return SKIP_BODY;
    }

    private String getFieldId() {
        return "combo_" + getField();
    }
}
