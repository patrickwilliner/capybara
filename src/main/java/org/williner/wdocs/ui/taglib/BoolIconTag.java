package org.williner.wdocs.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/01/14
 * Time: 13:43
 * To change this template use File | Settings | File Templates.
 */
public class BoolIconTag  extends SimpleTagSupport {
    private Boolean boolValue = Boolean.FALSE;

    public BoolIconTag() {
    }

    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();

        out.print("<span class=\"glyphicon glyphicon-");

        if (boolValue != null && boolValue) {
            out.print("ok\" />");
        } else {
            out.print("remove\" />");
        }
    }

    public void setBoolValue(Boolean boolValue) {
        this.boolValue = boolValue;
    }
}
