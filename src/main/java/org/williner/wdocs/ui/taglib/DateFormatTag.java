package org.williner.wdocs.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/01/14
 * Time: 21:22
 * To change this template use File | Settings | File Templates.
 */
public class DateFormatTag extends SimpleTagSupport {
    private final static String DEFAULT_DATE_FORMAT = "dd-mm-yyyy HH:mm:ss";

    private Date date;

    private String format = DEFAULT_DATE_FORMAT;

    public DateFormatTag() {
    }

    public void doTag() throws JspException, IOException {
        if (date != null) {
            JspWriter out = getJspContext().getOut();
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            out.print(sdf.format(date));
        }
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
