package org.williner.wdocs.ui.taglib;

import org.apache.commons.lang3.StringUtils;
import org.williner.wdocs.ui.ActionE;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 28/12/13
 * Time: 17:39
 * To change this template use File | Settings | File Templates.
 */
public class SidebarLinkTag extends TagSupport {
    private ActionE action;
    private String href;
    private String method;

    public void setAction(ActionE action) {
        this.action = action;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public int doStartTag() {
        try {
            JspWriter out = pageContext.getOut();

            out.print("<a href=\"");
            out.print(getUri());
            out.print("\" class=\"list-group-item");

            ActionE action = (ActionE)pageContext.getAttribute("action", PageContext.REQUEST_SCOPE);

            if (this.action.equals(action)) {
                out.print(" active");
            }

            out.print("\"");

            if (StringUtils.isNotEmpty(method)) {
                out.print("data-method=\"" + method.toLowerCase() + "\"");
            }

            out.print(">");
        } catch (Exception e) {
            throw new RuntimeException("", e);
        }

        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doEndTag(){
        try {
            JspWriter out = pageContext.getOut();
            out.println("</a>");
        } catch (Exception e){
            throw new RuntimeException("", e);
        }

        return SKIP_BODY;
    }

    private String getUri() {
        if (StringUtils.isNotEmpty(pageContext.getServletContext().getContextPath())) {
            return pageContext.getServletContext().getContextPath() + href;
        } else {
            return href;
        }
    }
}
