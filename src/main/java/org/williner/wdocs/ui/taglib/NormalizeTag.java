package org.williner.wdocs.ui.taglib;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/01/14
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
public class NormalizeTag extends SimpleTagSupport {
    private boolean empty;
    private boolean capitalize;

    public NormalizeTag() {
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public void setCapitalize(boolean capitalize) {
        this.capitalize = capitalize;
    }

    public void doTag() throws JspException, IOException {
        StringWriter sw = new StringWriter();
        getJspBody().invoke(sw);
        String innerText = sw.toString();
        StringBuffer text = new StringBuffer(innerText);

        if (empty && StringUtils.isEmpty(text)) {
            text = new StringBuffer("-");
        }

        if (capitalize && text.length() > 0) {
            text.replace(0, 1, "" + Character.toUpperCase(text.charAt(0)));
        }

        JspWriter out = getJspContext().getOut();
        out.print(text);
    }
}
