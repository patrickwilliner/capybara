package org.williner.wdocs.ui.taglib;

import org.williner.wdocs.ui.model.FlashMessage;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 13/01/14
 * Time: 21:56
 * To change this template use File | Settings | File Templates.
 */
public class FlashMessageTag extends MessageTagAbs {
    public FlashMessageTag() {
        super();
    }

    @Override
    public int doEndTag() {
        FlashMessage flashMessage = (FlashMessage)pageContext.getAttribute("message", PageContext.REQUEST_SCOPE);

        if (flashMessage != null) {
            String cssClass = null;

            switch(flashMessage.getType()) {
                case INFO:
                    cssClass = "alert-info";
                    break;
                case SUCCESS:
                    cssClass = "alert-success";
                    break;
                case WARNING:
                    cssClass = "alert-warning";
                    break;
                case ERROR:
                    cssClass = "alert-danger";
                    break;
                default:
                    throw new AssertionError("impossible");
            }

            String msg = getMessage(flashMessage.getMessage());

            JspWriter out = pageContext.getOut();
            try {
                out.print("<div class=\"alert alert-dismissable ");
                out.print(cssClass);
                out.print("\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>");
                out.print(msg);
                out.print("</div>");
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        return SKIP_BODY;
    }
}
