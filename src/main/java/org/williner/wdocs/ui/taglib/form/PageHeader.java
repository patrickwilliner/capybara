package org.williner.wdocs.ui.taglib.form;

import org.williner.wdocs.ui.taglib.MessageTagAbs;

import javax.servlet.jsp.JspWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 16/01/14
 * Time: 22:43
 * To change this template use File | Settings | File Templates.
 */
public class PageHeader extends MessageTagAbs {
    private String header;
    private String subheader;
    private Integer cardinality;

    public PageHeader() {
        super();
    }

    @Override
    public int doEndTag() {
        JspWriter out = pageContext.getOut();

        try {
            out.println("<div class=\"page-header\">");
            out.print("\t<h3>");
            out.print(getMessage(header));

            if (subheader != null) {
                out.print(" <small>");

                if (cardinality != null) {
                    out.print(cardinality);
                    out.print(" ");
                    out.print(pluralize(cardinality, getMessage(subheader)));
                } else {
                    out.print(getMessage(subheader));
                }

                out.print("</small>");
            }

            out.println("\t</h3></div>");
        } catch (IOException e) {
            throw new RuntimeException("", e);
        }

        return SKIP_BODY;
    }

    private static String pluralize(int cardinality, String text) {
        return cardinality == 1 ? text : text + "s";
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setSubheader(String subheader) {
        this.subheader = subheader;
    }

    public void setCardinality(Integer cardinality) {
        this.cardinality = cardinality;
    }
}
