package org.williner.wdocs.ui.taglib;


import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 15/01/14
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
public final class Utilities {
    private Utilities() {
    }

    public static String escapeJS(String value) {
        return StringEscapeUtils.escapeEcmaScript(value);
    }
}
