package org.williner.wdocs.ui.taglib.form;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.williner.wdocs.ui.taglib.MessageTagAbs;

import javax.servlet.jsp.PageContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 27/01/14
 * Time: 17:19
 * To change this template use File | Settings | File Templates.
 */
public class FormElementTagAbs extends MessageTagAbs {
    private String label;
    private boolean required = false;
    private Object bean;
    private String field;

    public FormElementTagAbs() {
        super();
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getLabel() {
        return label;
    }

    public boolean isRequired() {
        return required;
    }

    public Object getBean() {
        return bean;
    }

    public String getField() {
        return field;
    }

    protected String getValue() {
        if (bean != null) {
            BeanWrapper wrapper = new BeanWrapperImpl(bean);
            Object value = wrapper.getPropertyValue(field);

            if (value != null) {
                return value.toString();
            }
        }

        return "";
    }

    protected List<FieldError> getErrors() {
        List<ObjectError> allErrors = (List<ObjectError>)pageContext.getAttribute("errors", PageContext.REQUEST_SCOPE);
        List<FieldError> relevantErrors = new ArrayList();

        if (allErrors != null) {
            for (ObjectError error : allErrors) {
                if (error instanceof FieldError) {
                    FieldError fieldError = (FieldError)error;

                    if (StringUtils.equalsIgnoreCase(field, fieldError.getField())) {
                        relevantErrors.add(fieldError);
                    }
                }
            }
        }

        return relevantErrors;
    }
}
