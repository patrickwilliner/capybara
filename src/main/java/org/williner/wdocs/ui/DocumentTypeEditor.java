package org.williner.wdocs.ui;

import org.williner.wdocs.data.model.DocumentTypeDO;
import org.williner.wdocs.business.DocumentTypeManager;

import java.beans.PropertyEditorSupport;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 22/01/14
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */
public class DocumentTypeEditor extends PropertyEditorSupport {
    private DocumentTypeManager documentTypeManager;

    public DocumentTypeEditor(DocumentTypeManager documentTypeManager) {
        super();
        this.documentTypeManager = documentTypeManager;
    }

    public void setAsText(String text) {
        Long id = Long.parseLong(text);
        DocumentTypeDO documentType = documentTypeManager.findById(id);
        setValue(documentType);
    }
}
