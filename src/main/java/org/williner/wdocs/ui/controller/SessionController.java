package org.williner.wdocs.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 06/01/14
 * Time: 21:00
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class SessionController {

    @RequestMapping(value = "/login")
    public String doLogin(Model model) {
        model.addAttribute("content", "session/login");
        return "layouts/login";
    }
}
