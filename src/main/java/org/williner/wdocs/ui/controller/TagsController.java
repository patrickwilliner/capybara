package org.williner.wdocs.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.williner.wdocs.ui.ActionE;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 28/12/13
 * Time: 22:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value="/tags")
public class TagsController {

    @RequestMapping
    public String httpIndex(Model model) {
        ModelAndView mav = new ModelAndView();
        model.addAttribute("content", "tags/index");
        model.addAttribute("tags", Collections.EMPTY_LIST);
        model.addAttribute("action", ActionE.INDEX);

        return "layouts/application";
    }
}
