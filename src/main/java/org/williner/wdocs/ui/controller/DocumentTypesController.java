package org.williner.wdocs.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.williner.wdocs.business.DatatypeManager;
import org.williner.wdocs.business.DocumentTypeManager;
import org.williner.wdocs.data.model.*;
import org.williner.wdocs.ui.ActionE;
import org.williner.wdocs.ui.Meta;
import org.williner.wdocs.ui.model.FlashMessage;

import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 08/12/13
 * Time: 23:14
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value="/document_types")
@Meta(name = "document_types")
public class DocumentTypesController {
    @Autowired
    private DocumentTypeManager documentTypeManager;

    @Autowired
    private DatatypeManager datatypeManager;

    @RequestMapping
    public String doIndex(Model model) {
        model.addAttribute("content", "document_types/index");
        model.addAttribute("docTypes", documentTypeManager.findAll());
        model.addAttribute("action", ActionE.INDEX);
        return "layouts/application";
    }

    @RequestMapping(value="/new")
    public String httpNew(Model model) {
        model.addAttribute("content", "document_types/new");
        model.addAttribute("datatypes", datatypeManager.findAll());
        model.addAttribute("action", ActionE.NEW);
        return "layouts/application";
    }

    @RequestMapping(value="/{documentTypeId}")
    public String httpShow(@PathVariable Long documentTypeId, Model model) {
        DocumentTypeDO documentType = documentTypeManager.findById(documentTypeId);

        model.addAttribute("content", "document_types/show");
        model.addAttribute("documentType", documentType);
        model.addAttribute("action", ActionE.SHOW);
        return "layouts/application";
    }

    @RequestMapping(value="/{documentTypeId}/edit")
    public String doEdit(@PathVariable Long documentTypeId, Model model) {
        DocumentTypeDO documentType = documentTypeManager.findById(documentTypeId);

        model.addAttribute("content", "document_types/edit");
        model.addAttribute("documentType", documentType);
        model.addAttribute("datatypes", datatypeManager.findAll());
        model.addAttribute("action", ActionE.EDIT);
        return "layouts/application";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String httpCreate(@Valid DocumentTypeDO documentType, BindingResult result, SessionStatus status, Model model, RedirectAttributes attributes) {
        if (result.hasErrors()) {
            model.addAttribute("documentType", documentType);
            model.addAttribute("message", new FlashMessage(FlashMessage.TypeE.ERROR, "validation.has_errors"));
            model.addAttribute("errors", result.getAllErrors());
            return httpNew(model);
        } else {
            for (AttributeDO attr : documentType.getAttributes()) {
                attr.setDocumentType(documentType);
            }

            documentTypeManager.persist(documentType);

            status.isComplete();
            attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "New Document Type created."));
            return "redirect:/document_types";
        }
    }

    @RequestMapping(value = "/{documentTypeId}", method = RequestMethod.DELETE)
    public String httpDelete(@PathVariable Long documentTypeId, RedirectAttributes attributes) {
        documentTypeManager.remove(documentTypeId);

        attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "document_types.delete.success"));
        return "redirect:/document_types";
    }
}
