package org.williner.wdocs.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.williner.wdocs.data.model.DatatypeDO;
import org.williner.wdocs.business.DatatypeManager;
import org.williner.wdocs.ui.ActionE;
import org.williner.wdocs.ui.Meta;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 09/01/14
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Meta(name = "datatypes")
@RequestMapping(value="/datatypes")
public class DatatypesController {
    @Autowired
    private DatatypeManager datatypeManager;

    @RequestMapping
    public String httpIndex(Model model) {
        model.addAttribute("content", "datatypes/index");
        model.addAttribute("datatypes", datatypeManager.findAll());
        model.addAttribute("action", ActionE.INDEX);
        return "layouts/application";
    }

    @RequestMapping(value = "/{datatypeId}")
    public String httpShow(@PathVariable Long datatypeId, Model model) {
        DatatypeDO datatype = datatypeManager.findById(datatypeId);
        model.addAttribute("datatype", datatype);
        model.addAttribute("content", "datatypes/show");
        model.addAttribute("action", ActionE.SHOW);

        return "layouts/application";
    }
}
