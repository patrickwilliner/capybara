package org.williner.wdocs.ui.controller;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.williner.wdocs.business.RoleManager;
import org.williner.wdocs.data.model.UserDO;
import org.williner.wdocs.business.UserManager;
import org.williner.wdocs.data.validation.UserPasswordValidator;
import org.williner.wdocs.data.validation.UserValidator;
import org.williner.wdocs.ui.ActionE;
import org.williner.wdocs.ui.Meta;
import org.williner.wdocs.ui.model.User;
import org.williner.wdocs.ui.model.FlashMessage;
import org.williner.wdocs.util.HashUtil;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 14:07
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value="/users")
@Meta(name = "users")
public class UsersController {
    private final static String[] UPDATE_FIELDS = {
        "name", "login", "email", "role"
    };

    @Autowired
    private UserManager userManager;

    @Autowired
    private RoleManager roleManager;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping
    public String httpIndex(Model model) {
        model.addAttribute("content", "users/index");
        model.addAttribute("users", userManager.findAll());
        model.addAttribute("action", ActionE.INDEX);
        return "layouts/application";
    }

    @RequestMapping(value="/{userId}")
    public String httpShow(@PathVariable Long userId, Model model) {
        model.addAttribute("content", "users/show");
        model.addAttribute("action", ActionE.SHOW);
        model.addAttribute("user", userManager.findById(userId));
        return "layouts/application";
    }

    @RequestMapping(value="/new")
    public String httpNew(Model model) {
        model.addAttribute("content", "users/new");
        model.addAttribute("roles", roleManager.findAll());
        model.addAttribute("action", ActionE.NEW);
        return "layouts/application";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String httpCreate(UserDO user, Model model, RedirectAttributes attributes, BindingResult result) {
        userValidator.validate(user, result);

        if (result.hasErrors()) {
            model.addAttribute("errors", result.getAllErrors());
            model.addAttribute("message", new FlashMessage(FlashMessage.TypeE.ERROR, "validation.has_errors"));
            model.addAttribute("user", user);
            return httpNew(model);
        } else {
            user.setActive(false);
            user.setChangePassword(true);
            user.setSystem(false);
            userManager.persist(user);

            attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "New user created."));
            return "redirect:/users";
        }
    }

    @RequestMapping(value = "/{userId}/edit")
    public String httpEdit(@PathVariable Long userId, Model model) {
        UserDO user = (UserDO)model.asMap().get("user");
        if (user == null) {
            user = userManager.findById(userId);
        }

        model.addAttribute("content", "users/edit");
        model.addAttribute("action", ActionE.EDIT);
        model.addAttribute("user", user);
        model.addAttribute("roles", roleManager.findAll());
        return "layouts/application";
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.PATCH)
    public String httpUpdate(@PathVariable Long userId, UserDO user, Model model, RedirectAttributes attributes, BindingResult result) {
        user = loadAndMergeUser(user, userId);

        userValidator.validate(user, result);

        if (result.hasErrors()) {
            model.addAttribute("errors", result.getAllErrors());
            model.addAttribute("message", new FlashMessage(FlashMessage.TypeE.ERROR, "validation.has_errors"));
            model.addAttribute("user", user);
            return httpEdit(userId, model);
        } else {
            userManager.persist(user);
            attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "user.update.success"));
            return "redirect:/users/" + user.getId();
        }
    }

    @RequestMapping(value = "/{userId}/edit_pw")
    public String httpEditPassword(@PathVariable Long userId, Model model) {
        UserDO userDO = userManager.findById(userId);
        User user = new User(userId);
        user.setName(userDO.getName());

        model.addAttribute("content", "users/edit_pw");
        model.addAttribute("action", ActionE.EDIT_PASSWORD);
        model.addAttribute("user", user);

        return "layouts/application";
    }

    @RequestMapping(value = "/{userId}/edit_pw", method = RequestMethod.PATCH)
    public String httpUpdatePassword(User user, @PathVariable Long userId, BindingResult result, Model model, RedirectAttributes attributes) {
        user.setId(userId);

        UserPasswordValidator validator = new UserPasswordValidator();
        validator.validate(user, result);

        if (result.hasErrors()) {
            user.setPassword(null);
            user.setPasswordConfirmation(null);

            model.addAttribute("errors", result.getAllErrors());
            model.addAttribute("message", new FlashMessage(FlashMessage.TypeE.ERROR, "validation.has_errors"));

            return httpEditPassword(userId, model);
        } else {
            try {
                UserDO userDO = userManager.findById(user.getId());

                String hash = HashUtil.createHash(user.getPassword());
                userDO.setPasswordHash(hash);
                user.setPassword(null);
                user.setPasswordConfirmation(null);

                userManager.persist(userDO);

                attributes.addFlashAttribute("message", "user.update_pw.success");
                return "redirect:/users";
            } catch (Exception e) {
                throw new RuntimeException("", e);
            }
        }
    }

    @RequestMapping(value = "/{userId}/delete", method = RequestMethod.DELETE)
    public String httpDelete(@PathVariable Long userId, RedirectAttributes attributes) {
        userManager.remove(userId);

        attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "users.delete.success"));
        return "redirect:/users";
    }

    private UserDO loadAndMergeUser(UserDO user, Long userId) {
        UserDO dbUser = userManager.findById(userId);
        BeanWrapper dbWrapper = new BeanWrapperImpl(dbUser);
        BeanWrapper userWrapper = new BeanWrapperImpl(user);

        for (String field : UPDATE_FIELDS) {
            dbWrapper.setPropertyValue(field, userWrapper.getPropertyValue(field));
        }

        return dbUser;
    }
}
