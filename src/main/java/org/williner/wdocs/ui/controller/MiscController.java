package org.williner.wdocs.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 07/01/14
 * Time: 19:12
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class MiscController {

    @RequestMapping(value = "/")
    public String httpIndex(Model model) {
        model.addAttribute("content", "shared/welcome");
        return "layouts/application";
    }
}
