package org.williner.wdocs.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.williner.wdocs.business.DocumentManager;
import org.williner.wdocs.business.DocumentTypeManager;
import org.williner.wdocs.data.model.*;
import org.williner.wdocs.data.validation.DocumentValidator;
import org.williner.wdocs.ui.ActionE;
import org.williner.wdocs.ui.DocumentTypeEditor;
import org.williner.wdocs.ui.Meta;
import org.williner.wdocs.ui.model.AttributeInfo;
import org.williner.wdocs.ui.model.Document;
import org.williner.wdocs.ui.model.FlashMessage;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Spring MVC controller for documents.
 *
 * @author patrick.williner@gmail.com
 * @version 1.0 | 2014-01-22
 */
@Controller
@RequestMapping(value="/documents")
@Meta(name = "documents")
public class DocumentsController {
    @Autowired
    private DocumentTypeManager documentTypeManager;

    @Autowired
    private DocumentManager documentManager;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(DocumentTypeDO.class, new DocumentTypeEditor(documentTypeManager));
    }

    @RequestMapping
    public String httpIndex(Model model) {
        int page = 0;
        List<DocumentDO> documents = documentManager.findAll(page);
        long documentCount = documentManager.count();
        int pageCount = (int)Math.ceil((double)documentCount / (double)documentManager.getPageSize());

        model.addAttribute("content", "documents/index");
        model.addAttribute("documents", documents);
        model.addAttribute("documentCount", documentCount);
        model.addAttribute("pageCount", pageCount);
        model.addAttribute("action", ActionE.INDEX);
        model.addAttribute("page", page);

        return "layouts/application";
    }

    @RequestMapping(value = "/load_page/{page}")
    public String httpLoadPage(@PathVariable Integer page, Model model) {
        // TODO review - error prone
        long documentCount = documentManager.count();
        int pageCount = (int)Math.ceil((double)documentCount / (double)documentManager.getPageSize());

        model.addAttribute("documents", documentManager.findAll(page));
        model.addAttribute("page", page);
        model.addAttribute("pageCount", pageCount);

        return "documents/load_page";
    }

    @RequestMapping(value = "/{documentId}")
    public String httpShow(@PathVariable Long documentId, Model model) {
        DocumentDO document = documentManager.findById(documentId);

        model.addAttribute("content", "documents/show");
        model.addAttribute("document", document);
        model.addAttribute("action", ActionE.SHOW);

        return "layouts/application";
    }

    @RequestMapping(value="/search")
    public String httpSearch(DocumentSearchCriteria criteria, Model model) {
        List<DocumentDO> documents = new ArrayList<DocumentDO>();
        if (criteria.isSearch()) {
            documents = documentManager.find(criteria);
        }

        model.addAttribute("content", "documents/search");
        model.addAttribute("documents", documents);
        model.addAttribute("search_criteria", criteria);
        model.addAttribute("action", ActionE.SEARCH);
        return "layouts/application";
    }

    @RequestMapping(value="/new")
    public String httpNew(Document frmDocument, Model model, HttpServletRequest request) {
        Long documentTypeId = null;
        try {
            documentTypeId = Long.parseLong(request.getParameter("documentTypeId"));
        } catch (NumberFormatException ignored) {
        }

        initDocument(frmDocument, documentTypeId);

        model.addAttribute("content", "documents/new");
        model.addAttribute("document", frmDocument);
        model.addAttribute("document_types", documentTypeManager.findAll());
        model.addAttribute("attributeInfos", getAttributeInfos(frmDocument.getDocumentType()));
        model.addAttribute("action", ActionE.NEW);

        return "layouts/application";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String httpCreate(Document frmDocument, BindingResult result, Model model, RedirectAttributes attributes, HttpServletRequest request) {
        new DocumentValidator().validate(frmDocument, result);

        if (result.hasErrors()) {
            attributes.addFlashAttribute("errors", result.getAllErrors());
            attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.ERROR, "validation.has_errors"));
            attributes.addFlashAttribute("document", frmDocument);
            return "redirect:/documents/new";
        } else {
            DocumentDO documentDo = getDocument(frmDocument, null);
            documentManager.persist(documentDo);

            attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "New Document created."));
            return "redirect:/documents";
        }
    }

    @RequestMapping(value = "/{documentId}/edit")
    public String httpEdit(@PathVariable Long documentId, Model model) {
        Document frmDocument = loadDocument(documentId, model);

        model.addAttribute("content", "documents/edit");
        model.addAttribute("action", ActionE.EDIT);
        model.addAttribute("document", frmDocument);
        model.addAttribute("attributeInfos", getAttributeInfos(frmDocument.getDocumentType()));

        return "layouts/application";
    }

    @RequestMapping(value = "/{documentId}", method = RequestMethod.PATCH)
    public String httpUpdate(@PathVariable Long documentId, @Valid Document frmDocument, BindingResult result, Model model, RedirectAttributes attributes) {
        DocumentDO documentDo = documentManager.findById(documentId);
        frmDocument.setDocumentType(documentDo.getDocumentType());
        frmDocument.setId(documentDo.getId());

        new DocumentValidator().validate(frmDocument, result);

        if (result.hasErrors()) {
            attributes.addFlashAttribute("errors", result.getAllErrors());
            attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.ERROR, "validation.has_errors"));
            attributes.addFlashAttribute("document", frmDocument);
            return "redirect:/documents/" + frmDocument.getId() + "/edit";
        } else {
            DocumentDO document = getDocument(frmDocument, documentId);
            documentManager.persist(document);

            attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "documents.update.success"));
            return "redirect:/documents/" + document.getId();
        }
    }

    @RequestMapping(value = "/{documentId}", method = RequestMethod.DELETE)
    public String httpDelete(@PathVariable Long documentId, RedirectAttributes attributes) {
        documentManager.remove(documentId);

        attributes.addFlashAttribute("message", new FlashMessage(FlashMessage.TypeE.INFO, "documents.delete.success"));
        return "redirect:/documents";
    }

    private List<AttributeInfo> getAttributeInfos(DocumentTypeDO documentType) {
        List<AttributeInfo> attrInfos = new ArrayList();

        if (documentType != null) {
            for (AttributeDO attr : documentType.getAttributes()) {
                AttributeInfo attrInfo = new AttributeInfo(attr.getName(), attr.getRequired(), attr.getDatatype());
                attrInfos.add(attrInfo);
            }
        }

        return attrInfos;
    }

    private DocumentDO getDocument(Document frmDocument, Long documentId) {
        DocumentDO document = null;

        if (documentId != null) {
            document = documentManager.findById(documentId);
        } else {
            document = new DocumentDO();
            document.setDocumentType(frmDocument.getDocumentType());
        }

        for (int i = 0; i < document.getDocumentType().getAttributes().size(); i++) {
            document.getAttributeValues().get(i).setValue(frmDocument.getAttributeValues().get(i));
        }

        return document;
    }

    private Document loadDocument(Long documentId, Model model) {
        Document frmDocument = null;

        if (model.containsAttribute("document")) {
            frmDocument = (Document)model.asMap().get("document");
        } else {
            DocumentDO documentDo = documentManager.findById(documentId);
            frmDocument = new  Document();

            frmDocument.setDocumentType(documentDo.getDocumentType());
            frmDocument.setId(documentId);

            for (AttributeValueDO attrValue : documentDo.getAttributeValues()) {
                frmDocument.getAttributeValues().add(attrValue.getValue());
            }
        }


        return frmDocument;
    }

    private void initDocument(Document frmDocument, Long documentTypeId) {
        if (frmDocument.getDocumentType() == null) {
            DocumentTypeDO docType = null;

            if (documentTypeId != null) {
                docType = documentTypeManager.findById(documentTypeId);
            } else {
                docType = documentTypeManager.findAny();
            }

            if (docType != null) {
                frmDocument.setDocumentType(docType);

                for (AttributeDO attr : docType.getAttributes()) {
                    frmDocument.getAttributeValues().add("");
                }
            }
        } else {
            if (documentTypeId != null) {
                frmDocument.setDocumentType(documentTypeManager.findById(documentTypeId));
            }
        }
    }
}