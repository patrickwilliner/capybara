package org.williner.wdocs.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.williner.wdocs.business.RoleManager;
import org.williner.wdocs.ui.ActionE;
import org.williner.wdocs.ui.Meta;

/**
 * Created with IntelliJ IDEA.
 * User: wipa
 * Date: 29/12/13
 * Time: 14:07
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value="/roles")
@Meta(name = "roles")
public class RolesController {
    @Autowired
    private RoleManager roleManager;

    @RequestMapping
    public String httpIndex(Model model) {
        model.addAttribute("content", "roles/index");
        model.addAttribute("roles", roleManager.findAll());
        model.addAttribute("action", ActionE.INDEX);
        return "layouts/application";
    }

    @RequestMapping(value = "/{roleId}")
    public String httpShow(@PathVariable Long roleId, Model model) {
        model.addAttribute("content", "roles/show");
        model.addAttribute("role", roleManager.findById(roleId));
        model.addAttribute("action", ActionE.SHOW);
        return "layouts/application";
    }
}
